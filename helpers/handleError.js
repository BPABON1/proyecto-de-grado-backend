const httpError = (res, err)=>{
    console.error(err);
    res.status(500).json({
        ok: false,
        msg: 'Error, comuniquese con el administrador',
        error: err
    });
}

module.exports = {httpError};