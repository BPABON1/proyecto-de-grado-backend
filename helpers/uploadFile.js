const path = require('path');
const { v4: uuidv4 } = require('uuid');
// ⇨ '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed'

//poder gestionar la carga de archivos
const cargarArchivo = (files, extensionesValidas = ['jpg', 'png', 'pdf', 'jpeg'], carpeta = '') => {

    return new Promise((resolve, reject) => {

        //capturo el archivo
        const { archivo } = files;
        //separar el nombre del archivo 
        const cutName = archivo.name.split('.');
        //capturar la extension
        const extension = cutName[cutName.length - 1];


        //validar la extension
        if (!extensionesValidas.includes(extension)) {
            return reject(`La extension del archivo .${extension} no es permitida, las extensiones permitidas son: ${extensionesValidas}`);

        }

        //asignar un identificador al nombre del archivo
        const nombreTemp = uuidv4() + '.' + extension;

        //path completo del archivo
        const uploadPath = path.join(__dirname, '../uploads/', carpeta, nombreTemp);

        //mover el archivo al path
        archivo.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }

            resolve(nombreTemp);
        });

    });


}


module.exports = { cargarArchivo }