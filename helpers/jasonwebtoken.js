const jwt = require('jsonwebtoken');


//Es obligatorio para un usuario tener un token de valicdacion, sin este no puede interactuar en el sistema
const generarJWT = ( uid = '' ) => {
    return new Promise( (resolve, reject) => {

        const payload = { uid };

        jwt.sign( payload, process.env.JWT_SECRET, {
            expiresIn: '8h'
        }, ( err, token ) => {
            if ( err ) {
                console.log(err);
                reject( 'No se pudo generar el token' )
            } else {
                resolve( token );
            }
        })

    })
}
module.exports = {
    generarJWT
}
