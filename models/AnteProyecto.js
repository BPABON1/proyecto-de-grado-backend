const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const AnteProjectsUpload = db.define('AnteProjectsUpload', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    user_crea: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    user_modifica: {
        type: DataTypes.STRING,
        allowNull: true
    },
    id_proyecto: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    estado: {
        type: DataTypes.STRING,
        allowNull: false
    },
    estado_aprobacion: {
        type: DataTypes.STRING,
        allowNull: false
    },
}, {
    tableName: 'ante_proyectos_subidos',
});

module.exports = {
    AnteProjectsUpload
}