const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const { createServer } = require('http');

const { db } = require('../database/conexion');
class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.server = createServer(this.app);

        this.InicioSesion = '/api/project';

        //Conexion con bases de datos
        this.conectionBaseDatos();

        //Middlewares
        this.Validadores();

        //Rutas de la app
        this.rutas();
    }
    
    async conectionBaseDatos() {
        try {
            await db.authenticate();
        } catch (error) {
            throw new Error(error);
        }
    }

    Validadores() {
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.static('public'));
        this.app.use(fileUpload({
            useTempFiles: true,
            tempFileDir: '/tmp/',
            createParentPath: true
        }));
    }

    rutas() {
        this.app.use(this.InicioSesion, require('../routes/grado_autenticacion'));
    }

    listen() {
            this.server.listen(this.port, () => {
                console.log('Corriendo por el puerto: ', this.port);
            });
        }
}

module.exports = Server;