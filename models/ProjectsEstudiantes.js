const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const StudentProjects = db.define('StudentProjects', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    id_proyecto: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    id_estudiante: {
        type: DataTypes.INTEGER,
        allowNull: true, 
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    user_crea: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    user_modifica: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, {
    tableName: 'estudiantes_proyectos',
});

module.exports = {
    StudentProjects
}