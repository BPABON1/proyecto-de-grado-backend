const Usuarios = require('./grado_usuario');
const Roles = require('./grado_roles');
const Permisos = require('./grado_permisos');
const PermisosAsignados = require('./grado_permisos_asignados');
const Projects = require('./proyectos');
const StudentProjects = require('./ProjectsEstudiantes');
const ProjectsUpload = require('./ProyectosSubidos');
const DocumentosSubidos = require('./DocumentosSubidos');
const AnteProjectsUpload = require('./AnteProyecto');
const NotasProyecto = require('./NotasProyecto');
const NotasAnteProyecto = require('./NotasAnteProyecto');
const EvaluadoresProyectos = require('./EvaluadoresProyectos');
module.exports = {
    ...Usuarios,
    ...Roles,
    ...Permisos,
    ...PermisosAsignados,
    ...Projects,
    ...StudentProjects,
    ...ProjectsUpload,
    ...DocumentosSubidos,
    ...AnteProjectsUpload,
    ...NotasProyecto,
    ...NotasAnteProyecto,
    ...EvaluadoresProyectos,
}