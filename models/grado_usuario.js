const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const Usuarios = db.define('Usuarios', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    user: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: true
    },
    apellidos: {
        type: DataTypes.STRING,
        allowNull: true
    },
    estado: {
        type: DataTypes.STRING,
        allowNull: true
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    id_rol: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    identificacion:{
        type: DataTypes.STRING,
        allowNull: false
    },
    cambio_password:{
        type: DataTypes.STRING,
        allowNull: true
    },
    director_project:{
        type: DataTypes.STRING,
        allowNull: true
    },
    evaluador_project:{
        type: DataTypes.STRING,
        allowNull: true
    },
}, {
    tableName: 'usuario',
});

module.exports = {
    Usuarios
}