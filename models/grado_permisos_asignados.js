const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const PermisosAsignados = db.define('PermisosAsignados', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    id_permiso: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    id_rol: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    usuario_creo: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    usuario_mod: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_mod: {
        type: DataTypes.DATE,
        allowNull: true
    },
    estado: {
        type: DataTypes.STRING,
        allowNull: true
    },
}, {
    tableName: 'permisos_asignados',
});

module.exports = {
    PermisosAsignados
}