const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const Roles = db.define('Roles', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: true
    },
    usuario_creo: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    usuario_mod: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_mod: {
        type: DataTypes.DATE,
        allowNull: true
    },
    estado: {
        type: DataTypes.STRING,
        allowNull: true
    },
}, {
    tableName: 'rol',
});

module.exports = {
    Roles
}