const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const NotasProyecto = db.define('NotasProyecto', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    usuario_crea: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    usuario_modifica: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    id_proyecto : {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    nota: {
        type: DataTypes.STRING,
        allowNull: true
    },
    observacion_interna_evaluador:{
        type: DataTypes.TEXT,
        allowNull: true
    },
    recomendaciones :{
        type: DataTypes.TEXT,
        allowNull: true
    },
    id_documento:{
        type: DataTypes.INTEGER,
        allowNull: true 
    },
}, {
    tableName: 'notas_proyectos',
});

module.exports = {
    NotasProyecto
}