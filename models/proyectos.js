const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const Projects = db.define('Projects', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    titulo: {
        type: DataTypes.STRING,
        allowNull: false
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    user_crea: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    user_modifica: {
        type: DataTypes.STRING,
        allowNull: true
    },
    estado: {
        type: DataTypes.STRING,
        allowNull: false
    },
    descripccion:{
        type: DataTypes.TEXT,
        allowNull: true
    },
    etapa:{
        type: DataTypes.STRING,
        allowNull: true  
    }
}, {
    tableName: 'proyectos',
});

module.exports = {
    Projects
}