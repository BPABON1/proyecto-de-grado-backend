const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const EvaluadoresProyectos = db.define('EvaluadoresProyectos', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    id_proyecto: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    id_evaluador: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    user_crea: {
        type: DataTypes.STRING,
        allowNull: true
    },
    user_modifica: {
        type: DataTypes.STRING,
        allowNull: true
    },
    tipo: {
        type: DataTypes.STRING,
        allowNull: false
    },
    id_director:{
        type: DataTypes.INTEGER,
        allowNull: true
    }
}, {
    tableName: 'evaluadores_directores_proyectos',
});

module.exports = {
    EvaluadoresProyectos
}