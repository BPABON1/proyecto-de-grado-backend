const { db } = require('../database/conexion');
const { DataTypes } = require('sequelize');

const DocumentosSubidos = db.define('DocumentosSubidos', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
    },
    fecha_creacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    fecha_modificacion: {
        type: DataTypes.DATE,
        allowNull: true
    },
    user_entrego: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    user_modifica: {
        type: DataTypes.STRING,
        allowNull: true
    },
    estado: {
        type: DataTypes.STRING,
        allowNull: true
    },
    url_documento: {
        type: DataTypes.STRING,
        allowNull: true
    },
    id_proyecto_subido: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    publicar: {
        type: DataTypes.STRING,
        allowNull: true
    },
    /* usuario_aprobador: {
        type: DataTypes.INTEGER,
        allowNull: true
    }, 
    resumen: {
        type: DataTypes.TEXT,
        allowNull: true
    }, */
    /*observacion_interna_evaluador: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    recomendaciones_futuras: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    conclusion_publica: {
        type: DataTypes.TEXT,
        allowNull: true
    },  */ 
    tipo:{
        type: DataTypes.STRING,
        allowNull: true
    },
    id_ante_proyecto:{
        type: DataTypes.INTEGER,
        allowNull: true
    },
    descripcion:{
        type: DataTypes.TEXT,
        allowNull: true
    }
}, {
    tableName: 'documentos_subidos',
});

module.exports = {
    DocumentosSubidos
}