const { Sequelize } = require('sequelize');


//conexion con la base de datos del proyecto de forma local servidor en AME
const db = new Sequelize('grado', 'project', '123456', {
    host: 'localhost',
    dialect: 'mysql',
    charset: 'utf8',
    define: {
        charset: 'utf8',
        collate: 'utf8_general_ci',
        timestamps: false
    },
    dialectOptions: {
        useUTC: false,
        dateStrings: true,
        typeCast: true
    },
    timezone: '-05:00'
});


module.exports = {
    db,
};