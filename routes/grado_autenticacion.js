const { Router } = require('express');
const { check } = require('express-validator');
const {
    validarCampos,
    ExisteUsuario,
    ExisteProjects,
    ExisteSuperAdmin,
    validarJWT,
    ValidarEstudiantesProjects,
    ValidarExisteAsignacionProjects,
    ValidarEstudiantesAnteProjects,
    ExisteProfesorAnteProyecto,
    ExisteProfesorProyectoFinal,
    ExisteNotaProfesorAnteProyecto,
    ExisteNotaProfesorProyectoFinal,
    NotaAnteProyectoPendiente,
    NotaProyectoFinalPendiente,
} = require('../middlewares');
const { IniciarSesion,
    ListUserRoles,
    SaveUsuer,
    SaveProjects,
    UpdateProjects,
    ListStudentProjects,
    DeleteProjects,
    DetalleProjectsPersonal,
    UploadProjects,
    ListProjectEvaluar,
    DetalleProjectEvaluar,
    EvaluarEstadoProject,
    ListProjectArobados,
    UpdateEstadoPublico,
    ListProjecPublicados,
    DetalleProjecPublicados,
    UpdatePassword,
    NewPassword,
    DetalleAnteProjectsPersonal,
    DetalleProjectsFinalPersonal,
    UploadAnteProjects,
    UploadProjectsFinal,
    ListAnteProjectEvaluar,
    ListProjectFinalEvaluar,
    DetalleAnteProjectEvaluar,
    DetalleProjectFinalEvaluar,
    EvaluarAntePoyecto,
    EvaluarPoyectoFinal,
    UpdateUsuer } = require('../controllers/autenticacion');
    const { ListProjectAsignar,
        SaveAnteProjects,
        DeleteEstudianteAnteProyecto,
        DeleteDirecotrAnteProyecto,
        DeleteEvaluadorAnteProyecto,
        DeleteEvaluadorProyectoFinal,
        UpdateAnteProjects,
        DeleteDirecotrProyectoFinal,
        UpdateProjectsFinal,
    } = require('../controllers/cotroller');
const router = new Router();


router.put('/login/', [
    check('password', 'Se debe ingresar una contraseña valida ').not().isEmpty(),
    check('user', 'Se debe ingresar un usuario valido ').not().isEmpty(),
    validarCampos
], IniciarSesion);
/*
proposito: Cargar la lista de usuarios y los roles respectivos
*/
router.get('/list_user_rol/', ListUserRoles);
// Listar la informacion de los proyectos subidos y el titulo del actual 
router.get('/DetalleProjectPersonal/', [
    validarJWT],DetalleProjectsPersonal);
// Agregar el usuario a la base de datos con el respectivo ROL
router.post('/registerUser/',[
    validarJWT,
    check('identificacion', 'Se debe ingresar una identificación').not().isEmpty(),
    check('nombres', 'Se debe ingresar el nombre').not().isEmpty(),
    check('apellidos', 'Se debe ingresar los apellidos').not().isEmpty(),
    check('id_rol', 'Se debe ingresar un rol').not().isEmpty(),
    check('email', 'Se debe ingresar un correo electronico valido').not().isEmpty(),
    check('password', 'Se debe ingresar una contraseña').not().isEmpty(),
    check('identificacion').custom(ExisteUsuario),
    validarCampos
],SaveUsuer);
// Actualizar la informacion de los usuarios seleccionados
router.put('/UpdateUser/',[
    validarJWT,
    check('id', 'Identificación de usuario requerido').not().isEmpty(),
    check('identificacion', 'Se debe ingresar una identificación').not().isEmpty(),
    check('nombres', 'Se debe ingresar el nombre').not().isEmpty(),
    check('apellidos', 'Se debe ingresar los apellidos').not().isEmpty(),
    check('id_rol', 'Se debe ingresar un rol').not().isEmpty(),
    check('email', 'Se debe ingresar un correo electronico valido').not().isEmpty(),
    check('estado', 'Se debe ingresar un estado').not().isEmpty(),
    check('identificacion').custom(ExisteSuperAdmin),
    validarCampos
],UpdateUsuer);
/*
proposito: Cargar la lista de estudiantes y los proyectos creados en el sistema
*/
router.get('/list_student_projects/', ListStudentProjects);
// Agregar el usuario a la base de datos con el respectivo ROL
router.post('/registerProjects/',[
    validarJWT,
    check('titulo', 'El titulo del proyecto el obligatorio').not().isEmpty(),
    check('titulo').custom(ExisteProjects),
    validarCampos
],SaveProjects);
// Agregar el usuario a la base de datos con el respectivo ROL
router.post('/UpdateProjects/',[
    validarJWT,
    check('estado', 'Se debe ingresar un estado').not().isEmpty(),
    check('id', 'Se requiere el identificador de proyecto').not().isEmpty(),
    check('titulo', 'El titulo del proyecto el obligatorio').not().isEmpty(),
    validarCampos
],UpdateProjects);
// Eliminar la asociacion de estudiante con el proyecto seleccionado
router.put('/DeleteProjects/',[
    validarJWT,
    check('id_relacion', 'Se requiere el identificador').not().isEmpty(),
    validarCampos
],DeleteProjects);
// Realizar la entrega del proyecto de grado y subir el documento
router.put('/UploadProject/:idproject/:resumen/:concluciones/',[
    validarJWT,
],UploadProjects);
// Listar tesis de grado pendientes por evaluar
router.get('/ListProjectEvaluar/', [
    validarJWT],ListProjectEvaluar);
// Buscar el proyecto seleccionado a evaluar por el director
router.put('/DetalleProjectsEvaluar/',[
    validarJWT,
    check('id_proyecto', 'Se requiere el id del proyecto a evaluar').not().isEmpty(),
    validarCampos
],DetalleProjectEvaluar);
// Evaluar el proyecto de grado seleccionado agregando el estado evaluado mas las recomendaciones y observaciones 
router.put('/EvaluarEstadoProjects/',[
    validarJWT,
    check('id_project', 'Se requiere el id del proyecto a evaluar').not().isEmpty(),
    check('observacion_evaluador', 'La observación por parte del evaluador es obligatoria').not().isEmpty(),
    // check('recomendaciones_futuras', 'Las recomendaciones para proyectos futuros son obligatorias').not().isEmpty(),
    check('estado_aprobacion', 'El estado de aprobación del proyecto es obligatorio').not().isEmpty(),
    check('id_proyecto_seleccionado', 'El id del proyecto es obligatorio').not().isEmpty(),
    validarCampos
],EvaluarEstadoProject);
// Listar los proyectos de grado finales aprovados y listos para publicar
router.get('/ProjectsPublicos/', [
    validarJWT],ListProjectArobados);
// Cambiar el estado publico o privado de la tesis de grado aprovada previamente
router.put('/UpdatePublicoProjects/',[
    validarJWT,
    check('id_documento_subido', 'Se requiere el id del proyecto a evaluar').not().isEmpty(),
    check('publico', 'La observación por parte del evaluador es obligatoria').not().isEmpty(),
    validarCampos
],UpdateEstadoPublico);
// Listar los proyectos de grado finales aprobados y publicados
router.get('/ProjectsBuscadorAprobados/', [
    validarJWT],ListProjecPublicados);
// Cargar iformacion del proyecto de grado seleccionado
router.get('/DetalleProyectoPublicado/:id/', [
    validarJWT],DetalleProjecPublicados);
// Enviar correo con la nueva contraseña para su posterior cambio
router.put('/UpdatePassword/', [
    check('user', 'Se debe ingresar un usuario valido ').not().isEmpty(),
    validarCampos
], UpdatePassword);
// Enviar correo con la nueva contraseña para su posterior cambio
router.put('/NewPassword/', [
    check('password', 'Se debe ingresar una contraseña valida ').not().isEmpty(),
    check('id', 'Se debe ingresar el id del usuario').not().isEmpty(),
    validarCampos
], NewPassword);
/*
proposito: Cargar la lista proyectos con sus respectivas fases
*/
router.get('/AsignacionAnteProyectos/', ListProjectAsignar);
// Registrar ante proyecto
router.post('/SaveAnteProyecto/', [
    validarJWT,
    ValidarEstudiantesProjects,
    ValidarExisteAsignacionProjects,
], SaveAnteProjects);
// Eliminar el estudiante a cargo del ante proyecto
router.put('/DeleteEstudianteAnteProyectos/', [
    check('id_relacional', 'Se debe ingresar el id del estudiante a eliminar').not().isEmpty(),
    validarJWT,
    validarCampos
], DeleteEstudianteAnteProyecto);
// Eliminar el estudiante a cargo del ante proyecto
router.put('/DeleteDirectorAnteProyectos/', [
    check('id', 'Se debe ingresar el id del director a eliminar').not().isEmpty(),
    validarJWT,
    validarCampos
], DeleteDirecotrAnteProyecto);
// Eliminar el evaluaodr a cargo del ante proyecto
router.put('/DeleteEvaluadorAnteProyectos/', [
    check('id_relacional', 'Se debe ingresar el id del evaluador a eliminar').not().isEmpty(),
    validarJWT,
    validarCampos
], DeleteEvaluadorAnteProyecto);
// Actualizar ante proyecto
router.post('/UpdateAnteProyecto/', [
    check('titulo', 'El titulo del proyecto es obligatorio').not().isEmpty(),
    check('estado', 'El estado del proyecto es obligatorio').not().isEmpty(),
    validarCampos,
    validarJWT,
    ValidarEstudiantesAnteProjects,
], UpdateAnteProjects);
// Listar la informacion del ante proyecto pendiente por subir y su respectivo historial
router.get('/DetalleAnteProjectPersonal/', [
    validarJWT],DetalleAnteProjectsPersonal);
// Realizar la entrega del ante proyecto
router.put('/UploadAnteProject/:idproject/:observaciones/',[
    validarJWT,
],UploadAnteProjects);
// Listar ante proyectos pendientes por evaluar
router.get('/ListAnteProjectEvaluar/', [
    validarJWT],ListAnteProjectEvaluar);
// Buscar el ante proyecto seleccionado a evaluar por el director
router.put('/DetalleAnteProjectsEvaluar/',[
    validarJWT,
    check('id_proyecto', 'Se requiere el id del proyecto a evaluar').not().isEmpty(),
    validarCampos,
    ExisteNotaProfesorAnteProyecto
],DetalleAnteProjectEvaluar);
// Evaluar el ante proyecto seleccionado
router.put('/EvaluarAnteProjectsEvaluar/',[
    validarJWT,
    check('id_project', 'Se requiere el id del ante proyecto a evaluar').not().isEmpty(),
    check('observacion_evaluador', 'La observación del evaluador es requerido ').not().isEmpty(),
    check('nota_evaluador', 'Estado de calificación de proyecto es requerido ').not().isEmpty(),
    check('id_documento_entregado', 'La relación con el documento a evaluar es requerido').not().isEmpty(),
    check('id_proyecto_total', 'La relación con el el proyecto es requerida').not().isEmpty(),
    validarCampos,
    ExisteProfesorAnteProyecto,
    NotaAnteProyectoPendiente
],EvaluarAntePoyecto);
// Eliminar el director a cargo del proyecto final
router.put('/DeleteDirectorProyectosFinal/', [
    check('id', 'Se debe ingresar el id del director a eliminar').not().isEmpty(),
    validarJWT,
    validarCampos
], DeleteDirecotrProyectoFinal);
// Eliminar el evaluaodr a cargo del ante proyecto
router.put('/DeleteEvaluadorProyectosFinal/', [
    check('id_relacional', 'Se debe ingresar el id del evaluador a eliminar').not().isEmpty(),
    validarJWT,
    validarCampos
], DeleteEvaluadorProyectoFinal);
// Actualizar proyecto final
router.post('/UpdateProyectoFinal/', [
    check('titulo', 'El titulo del proyecto es obligatorio').not().isEmpty(),
    check('estado', 'El estado del proyecto es obligatorio').not().isEmpty(),
    validarCampos,
    validarJWT,
    ValidarEstudiantesAnteProjects,
], UpdateProjectsFinal);
// Listar la informacion del ante proyecto pendiente por subir y su respectivo historial
router.get('/DetalleProjectFinalPersonal/', [
    validarJWT],DetalleProjectsFinalPersonal);
// Realizar la entrega del proyecto final
router.put('/UploadProjectFinal/:idproject/:observaciones/',[
    validarJWT,
],UploadProjectsFinal);
// Listar proyectos finales pendientes por evaluar
router.get('/ListProjectFinalEvaluar/', [
    validarJWT],ListProjectFinalEvaluar);
// Buscar el proyecto Final seleccionado a evaluar por el director
router.put('/DetalleProjectsFinalEvaluar/',[
    validarJWT,
    check('id_proyecto', 'Se requiere el id del proyecto a evaluar').not().isEmpty(),
    validarCampos,
    ExisteNotaProfesorProyectoFinal,
],DetalleProjectFinalEvaluar);
// Evaluar el proyecto final seleccionado
router.put('/EvaluarProjectsFinalEvaluar/',[
    validarJWT,
    check('id_project', 'Se requiere el id del ante proyecto a evaluar').not().isEmpty(),
    check('observacion_evaluador', 'La observación del evaluador es requerido ').not().isEmpty(),
    check('nota_evaluador', 'Estado de calificación de proyecto es requerido ').not().isEmpty(),
    check('id_documento_entregado', 'La relación con el documento a evaluar es requerido').not().isEmpty(),
    check('id_proyecto_total', 'La relación con el el proyecto es requerida').not().isEmpty(),
    validarCampos,
    ExisteProfesorProyectoFinal,
    NotaProyectoFinalPendiente
],EvaluarPoyectoFinal);
module.exports = router;