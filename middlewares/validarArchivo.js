const {request, response} = require('express');


const validarArchivo = (req = request, res = response, next) => {

//Validaciones de existencias de archivos
//validar si en la req existe algun archivo para cargar si no existe status 400

if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
    return res.status(400).json({ 
        msg: 'No hay archivos para cargar'
     });
}
next();
}


module.exports = {validarArchivo}