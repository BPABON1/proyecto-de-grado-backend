const validarCampos = require('../middlewares/validarCampos');
const validarJWT = require('../middlewares/validarJWT');
const dbValidators = require('../middlewares/dbValidators');
const validarArchivo = require('../middlewares/validarArchivo');



module.exports = {
    ...validarCampos,
    ...validarJWT,
    ...dbValidators,
    ...validarArchivo,
}