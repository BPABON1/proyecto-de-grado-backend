const { Op } = require("sequelize");
const {
    Usuarios,
    Projects,
    StudentProjects,
    NotasAnteProyecto,
    DocumentosSubidos,
    NotasProyecto,
} = require('../models/');

//Es necesario validar todos los datos de ingreso antes de enviarlos a la base de datos de

const ExisteUsuario = async(identificacion = '') => {
    const Existe = await Usuarios.findOne({ where: { user: identificacion } });
    if (Existe) {
        throw new Error(`El usuario: ${identificacion} ya esta registrado en la BD`);
    }
}
const ExisteProjects = async(titulo = '') => {
    const Existe = await Projects.findOne({ where: { titulo: titulo } });
    if (Existe) {
        throw new Error(`El proyecto: ${titulo} ya existe`);
    }
}
// Validar que el usuario que esta tratando de actualizar no sea el super administrador 
const ExisteSuperAdmin = async(identificacion = '') => {
    if (identificacion === '1095825459') {
        throw new Error(`El usuario: ${identificacion} No se puede actualizar por que tiene las credenciales de super administrador`);
    }
}
const ValidarEstudiantesProjects = async (req, res, next) => {
    let existeProject = 'NO';
    let mensaje = '';
    for(let value of req.body.estudiantes){
        let Existe = await StudentProjects.findOne({ where: { id_estudiante: value.id } });
        if(Existe){
            existeProject = 'SI';
            mensaje = `El estudiante: ${value.nombre} ya esta asignado a un proyecto `;
        }
    }
    if(existeProject === 'SI'){
        res.status(404).json({
            msg: mensaje
          });
    }else{
        next();
    }
}
const ValidarExisteAsignacionProjects = async (req, res, next) => {
    const Existe = await StudentProjects.findOne({ where: { id_proyecto: req.body.titulo[0].id } });        
    if(Existe){
        res.status(404).json({
            msg: `El proyecto: ${req.body.titulo[0].nombre} ya esta asignado a unos estudiantes `,
          });
    }else{
        next();
    }
}
const ValidarEstudiantesAnteProjects = async (req, res, next) => {
    let existeProject = 'NO';
    let mensaje = '';
    for(let value of req.body.estudiantes){
        if(value.bandera === 'NUEVO'){
            let Existe = await StudentProjects.findOne({ where: { id_estudiante: value.id } });
            console.log(Existe);
            if(Existe){
                existeProject = 'SI';
                mensaje = `El estudiante: ${value.nombre} ya esta asignado a un proyecto `;
            }
        }
    }
    if(existeProject === 'SI'){
        res.status(404).json({
            msg: mensaje
          });
    }else{
        next();
    }
}
const ExisteProfesorAnteProyecto = async (req, res, next) => {
    let Existe = await NotasAnteProyecto.findOne({ where: {
        usuario_crea: req.usuario.id,
        id_ante_proyecto: req.body.id_project,
        id_documento: req.body.id_documento_entregado
    } });
    // console.log(Existe);
    if(Existe){
        res.status(404).json({
            msg:  `El usuario ${req.usuario.nombre} ${req.usuario.apellidos} ya asigno un estado de calificación `
            });
    }else{
        next();
    }
}
const NotaAnteProyectoPendiente = async (req, res, next) => {
    let Existe = await  DocumentosSubidos.findOne({
        where: {
            id: req.body.id_documento_entregado,
            estado:{[Op.ne]: null},
        }
    });
    if(Existe){
        res.status(404).json({
            msg:  `El ante proyecto ya cuenta con una nota establecida por los evaluadores`
            });
    }else{
        next();
    }
}
const ExisteNotaProfesorAnteProyecto = async (req, res, next) => {
    const Documento = await DocumentosSubidos.findOne({
        where: {
            tipo: 'ANTE PROYECTO',
            id_ante_proyecto: req.body.id_proyecto,
            estado: null
        }
    });
    if(Documento){
        let Existe = await NotasAnteProyecto.findOne({ where: {
            usuario_crea: req.usuario.id,
            id_ante_proyecto: req.body.id_proyecto,
            id_documento: Documento.id
        } });
        // console.log(Existe);
        if(Existe){
            res.status(404).json({
                msg:  `El usuario ${req.usuario.nombre} ${req.usuario.apellidos} ya asigno un estado de calificación `
                });
        }else{
            next();
        }
    }else{
        res.status(404).json({
            msg: `No fue posible establecer una relación con el documento entregado`
        });
    }
}
const ExisteNotaProfesorProyectoFinal = async (req, res, next) => {
    const Documento = await DocumentosSubidos.findOne({
        where: {
            tipo: 'PROYECTO FINAL',
            id_proyecto_subido: req.body.id_proyecto,
            estado: null
        }
    });
    if(Documento){
        let Existe = await NotasProyecto.findOne({ where: {
            usuario_crea: req.usuario.id,
            id_proyecto: req.body.id_proyecto,
            id_documento: Documento.id
        } });
        // console.log(Existe);
        if(Existe){
            res.status(404).json({
                msg:  `El usuario ${req.usuario.nombre} ${req.usuario.apellidos} ya asigno un estado de calificación `
                });
        }else{
            next();
        }
    }else{
        res.status(404).json({
            msg: `No fue posible establecer una relación con el documento entregado`
        });
    }
}
const ExisteProfesorProyectoFinal = async (req, res, next) => {
    let Existe = await NotasProyecto.findOne({ where: {
        usuario_crea: req.usuario.id,
        id_proyecto: req.body.id_project,
        id_documento: req.body.id_documento_entregado
    } });
    // console.log(Existe);
    if(Existe){
        res.status(404).json({
            msg:  `El usuario ${req.usuario.nombre} ${req.usuario.apellidos} ya asigno un estado de calificación `
            });
    }else{
        next();
    }
}
const NotaProyectoFinalPendiente = async (req, res, next) => {
    let Existe = await  DocumentosSubidos.findOne({
        where: {
            id: req.body.id_documento_entregado,
            estado:{[Op.ne]: null},
        }
    });
    if(Existe){
        res.status(404).json({
            msg:  `El proyecto ya cuenta con una nota establecida por los evaluadores`
            });
    }else{
        next();
    }
}
module.exports = {
    ExisteUsuario,
    ExisteProjects,
    ExisteSuperAdmin,
    ValidarEstudiantesProjects,
    ValidarExisteAsignacionProjects,
    ValidarEstudiantesAnteProjects,
    ExisteProfesorAnteProyecto,
    NotaAnteProyectoPendiente,
    ExisteNotaProfesorAnteProyecto,
    ExisteNotaProfesorProyectoFinal,
    ExisteProfesorProyectoFinal,
    NotaProyectoFinalPendiente,
}