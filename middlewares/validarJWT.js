const { request, response, NextFunction } = require('express');
const jwt = require('jsonwebtoken');
const { Usuarios } = require('../models/grado_usuario');


//Se requiere que cuando el usuario este en uso de app  se valide que el token sea correto
const validarJWT = async (req = request, res = response, NextFunction) => {
    try {
        //leer el token
        const token = req.header('x-token');

        if (!token) {
            return res.status(401).json({
                ok: false,
                msg: 'No existe token'
            })
        } else {
            const { uid } = jwt.verify(token, process.env.JWT_SECRET);
            req.params.uid = uid;

            //leer el usuario que corresponde al uid
            const usuario = await Usuarios.findByPk(uid);

            //validar existencia del usuario
            if (!usuario) {
                return res.status(401).json({
                    msg: 'Usuario no existe'
                })
            } else {
                //verificar estado del usuario
                if (usuario.estado === 'Inactivo') {
                    return res.status(401).json({
                        msg: 'Token invalido'
                    })
                } else {
                    req.usuario = usuario;
                    NextFunction();
                }
            }
        }
    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Por favor verifique que la sesión este iniciada'
        })
    }
}

module.exports = {
    validarJWT
}