const { request, response, NextFunction } = require('express');
const { validationResult } = require('express-validator');


//se requiere validar los campos esten validados antes de enviarse a la base de datos
const validarCampos = (req, res, NextFunction) => {

    const error = validationResult(req);
    if (!error.isEmpty()) {
        return res.status(400).json({
            ok: false,
            error: error.array()
        });

    }
    NextFunction();
}

module.exports = { validarCampos };