-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.4.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para grado
CREATE DATABASE IF NOT EXISTS `grado` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `grado`;

-- Volcando estructura para tabla grado.ante_proyectos_subidos
CREATE TABLE IF NOT EXISTS `ante_proyectos_subidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `user_crea` int(11) DEFAULT NULL COMMENT 'Id del usuario que creo el registro',
  `user_modifica` int(11) DEFAULT NULL COMMENT 'Id del ultimo usuario en modificar el registro',
  `id_proyecto` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla de proyectos para poder determinar el titulo del proyecto que se subio',
  `estado` varchar(50) DEFAULT NULL COMMENT 'Estado del registro \r\nActivo \r\nInactivo',
  `estado_aprobacion` varchar(12) DEFAULT NULL COMMENT 'Me determina el estado de aprobacion del proyecto de grado entregado\r\nAPROBADO\r\nRECHAZADO\r\nPENDIENTE\r\nCORREGIR',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `PROYECTO_UNICO` (`estado_aprobacion`,`id_proyecto`) USING BTREE,
  KEY `FK_RELACION_ANTE_PROYECTO` (`id_proyecto`) USING BTREE,
  KEY `FK4_USUARIO_CREADOR` (`user_crea`) USING BTREE,
  KEY `FK5_USUARIO_MODIFICA` (`user_modifica`) USING BTREE,
  CONSTRAINT `FK_RELACION_ANTE_PROYECTO` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='Historial de total de ate proyectos subidos al sistema para su posterior  calificacion .\r\n';

-- Volcando datos para la tabla grado.ante_proyectos_subidos: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `ante_proyectos_subidos` DISABLE KEYS */;
INSERT INTO `ante_proyectos_subidos` (`id`, `fecha_creacion`, `fecha_modificacion`, `user_crea`, `user_modifica`, `id_proyecto`, `estado`, `estado_aprobacion`) VALUES
	(3, '2022-10-30 05:43:00', '2022-11-06 14:37:00', 1, 3, 7, 'Activo', 'APROBADO');
/*!40000 ALTER TABLE `ante_proyectos_subidos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.documentos_subidos
CREATE TABLE IF NOT EXISTS `documentos_subidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `user_entrego` int(11) DEFAULT NULL COMMENT 'Id del usuario que entrego el proyecto de grado',
  `user_modifica` int(11) DEFAULT NULL COMMENT 'Id del ultimo usuario en modificar el registro',
  `estado` varchar(12) DEFAULT NULL COMMENT 'Determina el estado de aprobacion del proyecto entregado\r\nAprobado \r\nRechazado\r\nnull = Pendiente',
  `url_documento` text DEFAULT NULL COMMENT 'Ruta url de donde se encuentra almacenado el documento PDF',
  `id_proyecto_subido` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla proyectos_subidos ',
  `publicar` varchar(2) DEFAULT NULL COMMENT 'Determina si el proyecto se debe publicar (SI,NO) para ser consultados por los demas estudiantes',
  `tipo` varchar(50) DEFAULT NULL COMMENT 'Determina el tipo de documento que se esta subiendo\r\n- ANTE PROYECTO\r\n- PROYECTO FINAL',
  `id_ante_proyecto` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla ante_proyectos_subidos ',
  `descripcion` text DEFAULT NULL COMMENT 'Descripción de la entrega del ante proyecto ',
  PRIMARY KEY (`id`),
  KEY `FK_usuario_entrego_tesis` (`user_entrego`),
  KEY `FK2_usuario_modifico_tesis` (`user_modifica`),
  KEY `FK3_proyectos_subidos` (`id_proyecto_subido`),
  KEY `FK5_ante_proyectos` (`id_ante_proyecto`),
  CONSTRAINT `FK3_proyectos_subidos` FOREIGN KEY (`id_proyecto_subido`) REFERENCES `proyectos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK5_ante_proyectos` FOREIGN KEY (`id_ante_proyecto`) REFERENCES `ante_proyectos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_usuario_entrego_tesis` FOREIGN KEY (`user_entrego`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COMMENT='Historial de documentos subidos a la plataforma web';

-- Volcando datos para la tabla grado.documentos_subidos: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `documentos_subidos` DISABLE KEYS */;
INSERT INTO `documentos_subidos` (`id`, `fecha_creacion`, `fecha_modificacion`, `user_entrego`, `user_modifica`, `estado`, `url_documento`, `id_proyecto_subido`, `publicar`, `tipo`, `id_ante_proyecto`, `descripcion`) VALUES
	(21, '2022-10-30 05:43:00', '2022-11-06 02:08:00', 1, 11, 'Rechazado', 'C:\\xampp\\htdocs\\grado\\proyecto-de-grado-backend\\uploads\\279120ff-3e85-4975-bf14-f273c4169d2f.pdf', NULL, NULL, 'ANTE PROYECTO', 3, 'descripcion prueba ante proyecto'),
	(27, '2022-11-06 14:37:00', '2022-11-06 14:45:00', 3, 5, 'Aprobado', 'C:\\xampp\\htdocs\\grado\\proyecto-de-grado-backend\\uploads\\4227fcf3-4a59-4441-8fe4-b279de8bf472.pdf', NULL, NULL, 'ANTE PROYECTO', 3, 'PRUEBA 5'),
	(28, '2022-11-07 08:29:00', '2022-11-07 12:24:00', 3, 8, 'Rechazado', 'C:\\xampp\\htdocs\\grado\\proyecto-de-grado-backend\\uploads\\97068228-bfd1-42be-b1b7-2bf7c35a2ccf.pdf', 20, NULL, 'PROYECTO FINAL', NULL, 'Proyecto subido de prueba entrega proyecto final'),
	(29, '2022-11-07 12:25:00', '2022-11-07 12:27:00', 3, 3, 'Aprobado', 'C:\\xampp\\htdocs\\grado\\proyecto-de-grado-backend\\uploads\\abe4be6f-2104-44fb-b3b5-f1588056242a.pdf', 20, 'SI', 'PROYECTO FINAL', NULL, 'se ajusto la metodologia');
/*!40000 ALTER TABLE `documentos_subidos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.estudiantes_proyectos
CREATE TABLE IF NOT EXISTS `estudiantes_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico del registro',
  `id_proyecto` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla proyectos ',
  `id_estudiante` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `user_crea` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario y me permite saber cual usuario creo el registro',
  `user_modifica` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario y me permite saber el ultimo usuario en modificar el dato',
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects estudiantes` (`id_proyecto`,`id_estudiante`),
  KEY `estudiantes` (`id_estudiante`),
  KEY `usuario_creador` (`user_crea`),
  KEY `ultimo_usuario_modifica` (`user_modifica`),
  CONSTRAINT `estudiantes` FOREIGN KEY (`id_estudiante`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `proyectos` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COMMENT='Me relaciona el proyecto con los estudiantes a cargo de la subida del documento para su posterior evaluacion ';

-- Volcando datos para la tabla grado.estudiantes_proyectos: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `estudiantes_proyectos` DISABLE KEYS */;
INSERT INTO `estudiantes_proyectos` (`id`, `id_proyecto`, `id_estudiante`, `fecha_creacion`, `fecha_modificacion`, `user_crea`, `user_modifica`) VALUES
	(20, 7, 3, '2022-10-29 07:55:00', NULL, 1, NULL),
	(23, 8, 7, '2022-11-06 16:08:00', NULL, 3, NULL),
	(24, 7, 2, '2022-11-06 16:42:00', NULL, 3, NULL);
/*!40000 ALTER TABLE `estudiantes_proyectos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.evaluadores_directores_proyectos
CREATE TABLE IF NOT EXISTS `evaluadores_directores_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico del registro',
  `id_evaluador` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario para determinar el usuario que puede evaluar el respectivo proyecto',
  `id_proyecto` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla proyectos ',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `user_crea` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario y me permite saber cual usuario creo el registro',
  `user_modifica` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario y me permite saber el ultimo usuario en modificar el dato',
  `tipo` varchar(25) DEFAULT NULL COMMENT 'Diferentes tipos de evaluaciones ANTE PROYECTO\r\n- PROYECTO FINAL',
  `id_director` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuarios y me permite determinar el director del \r\ntipo ANTE PROYECTO - PROYECTO FINAL',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `projects evaluadores` (`id_proyecto`,`id_evaluador`,`tipo`) USING BTREE,
  KEY `evaluador2` (`id_evaluador`) USING BTREE,
  KEY `usuario_creador_evaluador` (`user_crea`) USING BTREE,
  KEY `ultimo_usuario_modifica_evaluador` (`user_modifica`) USING BTREE,
  CONSTRAINT `evaluador2` FOREIGN KEY (`id_evaluador`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `proyectos2` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='Me relaciona el proyecto con los evaluadores a cargo de evaluar los respectivos proyectos';

-- Volcando datos para la tabla grado.evaluadores_directores_proyectos: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `evaluadores_directores_proyectos` DISABLE KEYS */;
INSERT INTO `evaluadores_directores_proyectos` (`id`, `id_evaluador`, `id_proyecto`, `fecha_creacion`, `fecha_modificacion`, `user_crea`, `user_modifica`, `tipo`, `id_director`) VALUES
	(4, 1, 7, '2022-10-30 08:09:00', NULL, 1, NULL, 'ANTE PROYECTO', 8),
	(5, 11, 7, '2022-10-30 08:12:00', NULL, 1, NULL, 'ANTE PROYECTO', 8),
	(6, 5, 7, '2022-10-30 08:12:00', NULL, 1, NULL, 'ANTE PROYECTO', 8),
	(7, 1, 8, '2022-11-06 16:08:00', NULL, 3, NULL, 'ANTE PROYECTO', 5),
	(8, 8, 8, '2022-11-06 16:08:00', NULL, 3, NULL, 'ANTE PROYECTO', 5),
	(9, 11, 8, '2022-11-06 16:08:00', NULL, 3, NULL, 'ANTE PROYECTO', 5),
	(10, 8, 7, '2022-11-06 16:42:00', NULL, 3, NULL, 'PROYECTO FINAL', 5),
	(11, 11, 7, '2022-11-06 16:42:00', NULL, 3, NULL, 'PROYECTO FINAL', 5),
	(13, 1, 7, '2022-11-06 17:05:00', NULL, 3, NULL, 'PROYECTO FINAL', 5);
/*!40000 ALTER TABLE `evaluadores_directores_proyectos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.notas_ante_proyectos
CREATE TABLE IF NOT EXISTS `notas_ante_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se realizo el insert del registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `usuario_crea` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla usuarios para poder determinar el usuario que realizo el insert del registro\r\n(Docente que realizo la nota)',
  `usuario_modifica` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario \r\nAlmacena el id del ultimo usuario en modificar el registro',
  `id_ante_proyecto` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla ante_proyectos_subidos para poder relacionar las nota con el proyecto el cual fue entregado por el estudiante',
  `nota` varchar(25) DEFAULT NULL COMMENT 'Diferentes tipos de notas\r\n- APROBADO\r\n- APROBADO CON CORRECCIONES\r\n- PENDIENTE DE APROBACION\r\n- NO APROBADO\r\n- ANULADO',
  `observacion_interna_evaluador` text DEFAULT NULL COMMENT 'Observaciones agregadas por el evaluador encargado',
  `id_documento` int(11) DEFAULT NULL COMMENT 'Relación con la tabla documentos_subidos ',
  `recomendaciones` text DEFAULT NULL COMMENT 'Recomendaciones suministradas por el evaluador ',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK4_usuario_creador` (`usuario_crea`) USING BTREE,
  KEY `FK5_usuario_modificador` (`usuario_modifica`) USING BTREE,
  KEY `FK6_ante_proyecto_entregado` (`id_ante_proyecto`) USING BTREE,
  KEY `FK4_documentos` (`id_documento`),
  CONSTRAINT `FK4_documentos` FOREIGN KEY (`id_documento`) REFERENCES `documentos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK4_usuario_creador` FOREIGN KEY (`usuario_crea`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK5_usuario_modificador` FOREIGN KEY (`usuario_modifica`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK6_ante_proyecto_entregado` FOREIGN KEY (`id_ante_proyecto`) REFERENCES `ante_proyectos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='Notas de los diferentes evaluadores tabla relacionado con los proyectos subidos';

-- Volcando datos para la tabla grado.notas_ante_proyectos: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `notas_ante_proyectos` DISABLE KEYS */;
INSERT INTO `notas_ante_proyectos` (`id`, `fecha_creacion`, `fecha_modificacion`, `usuario_crea`, `usuario_modifica`, `id_ante_proyecto`, `nota`, `observacion_interna_evaluador`, `id_documento`, `recomendaciones`) VALUES
	(1, '2022-11-06 01:38:00', NULL, 1, NULL, 3, 'PENDIENTE DE APROBACION', 'observacon de evaluador', 21, 'recomendaciones futuras'),
	(3, '2022-11-06 02:08:00', NULL, 11, NULL, 3, 'PENDIENTE DE APROBACION', 'falta ajustar metodologia ', 21, 'arreglar'),
	(4, '2022-11-06 14:38:00', NULL, 1, NULL, 3, 'PENDIENTE DE APROBACION', 'PRUEBA 1', 27, ''),
	(7, '2022-11-06 14:44:00', NULL, 11, NULL, 3, 'NO APROBADO', 'no pasa', 27, ''),
	(8, '2022-11-06 14:45:00', NULL, 5, NULL, 3, 'APROBADO', 'revisandolo bien si cumple con los objetivos', 27, '');
/*!40000 ALTER TABLE `notas_ante_proyectos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.notas_proyectos
CREATE TABLE IF NOT EXISTS `notas_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se realizo el insert del registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `usuario_crea` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla usuarios para poder determinar el usuario que realizo el insert del registro\r\n(Docente que realizo la nota)',
  `usuario_modifica` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla usuario \r\nAlmacena el id del ultimo usuario en modificar el registro',
  `id_proyecto` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla proyectos_subidos para poder relacionar las nota con el proyecto el cual fue entregado por el estudiante',
  `nota` varchar(25) DEFAULT NULL COMMENT 'Diferentes tipos de notas\r\n- APROBADO\r\n- APROBADO CON CORRECCIONES\r\n- PENDIENTE DE APROBACION\r\n- NO APROBADO\r\n- ANULADO',
  `observacion_interna_evaluador` text DEFAULT NULL COMMENT 'Observaciones agregadas por el evaluador encargado',
  `recomendaciones` text DEFAULT NULL COMMENT 'Recomendaciones para futuros proyectos',
  `id_documento` int(11) DEFAULT NULL COMMENT 'Relación con la tabla documentos_subidos ',
  PRIMARY KEY (`id`),
  KEY `FK1_usuario_creador` (`usuario_crea`),
  KEY `FK2_usuario_modificador` (`usuario_modifica`),
  KEY `FK3_proyecto_entregado` (`id_proyecto`),
  KEY `FK4_id_documento` (`id_documento`),
  CONSTRAINT `FK1_usuario_creador` FOREIGN KEY (`usuario_crea`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK2_usuario_modificador` FOREIGN KEY (`usuario_modifica`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK3_proyecto_entregado` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK4_id_documento` FOREIGN KEY (`id_documento`) REFERENCES `documentos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Notas de los diferentes evaluadores tabla relacionado con los proyectos subidos';

-- Volcando datos para la tabla grado.notas_proyectos: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `notas_proyectos` DISABLE KEYS */;
INSERT INTO `notas_proyectos` (`id`, `fecha_creacion`, `fecha_modificacion`, `usuario_crea`, `usuario_modifica`, `id_proyecto`, `nota`, `observacion_interna_evaluador`, `recomendaciones`, `id_documento`) VALUES
	(1, '2022-11-07 12:22:00', NULL, 1, NULL, 20, 'PENDIENTE DE APROBACION', 'primera evaluacion sin aprobar ', 'recomendacion 1', 28),
	(2, '2022-11-07 12:24:00', NULL, 8, NULL, 20, 'PENDIENTE DE APROBACION', 'observacon 2 mandar a corregir ', 'corregir metodologia ', 28),
	(3, '2022-11-07 12:26:00', NULL, 1, NULL, 20, 'APROBADO', 'perfecto', 'Aplicar software a la nube', 29),
	(4, '2022-11-07 12:27:00', NULL, 11, NULL, 20, 'APROBADO', 'perfecto publicar proyecto', 'Cifrar la base de datos ', 29);
/*!40000 ALTER TABLE `notas_proyectos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre del permiso que se usara en el sistema',
  `usuario_creo` int(11) DEFAULT NULL COMMENT 'Id del usuario que creo el registro',
  `usuario_mod` int(11) DEFAULT NULL COMMENT 'Id del ultimo usuario en modificar el registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_mod` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en ser modificado el registro',
  `estado` varchar(10) DEFAULT NULL COMMENT 'Determinara el estado del registro \r\nActivo \r\nInactivo',
  `codigo` varchar(30) DEFAULT NULL COMMENT 'Codigo del permiso este sera el que se valide en el frot end',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Índice 4` (`codigo`),
  KEY `FK_usuario_crea_permiso` (`usuario_creo`),
  KEY `FK_usuario_modifica_permiso` (`usuario_mod`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='Listado de permisos que se usaran en el sistema';

-- Volcando datos para la tabla grado.permisos: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`id`, `nombre`, `usuario_creo`, `usuario_mod`, `fecha_creacion`, `fecha_mod`, `estado`, `codigo`) VALUES
	(1, 'Lista de usuarios', 1, 1, '2022-03-13 13:53:24', '2022-03-13 13:53:26', 'Activo', 'list_user'),
	(2, 'Asignacion de proyectos a estudiantes', 1, 1, '2022-04-15 14:58:39', '2022-04-15 14:58:39', 'Activo', 'admin_projects'),
	(3, 'Entregar tesis de grado', 1, 1, '2022-04-18 18:14:59', '2022-04-18 18:15:00', 'Activo', 'entregar_tesis'),
	(4, 'Evaluar tesis de grado', 1, 1, '2022-05-01 13:31:55', '2022-05-01 13:31:56', 'Activo', 'evaluar_tesis'),
	(5, 'Publicar proyectos aprobados', 1, 1, '2022-05-08 11:50:48', '2022-05-08 11:50:50', 'Activo', 'publicar_tesis'),
	(6, 'Asignar estudiantes y evaluadores', 1, 1, '2022-10-26 21:04:48', '2022-10-26 21:04:49', 'Activo', 'asignar_usuarios');
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.permisos_asignados
CREATE TABLE IF NOT EXISTS `permisos_asignados` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico del registro',
  `id_permiso` int(11) DEFAULT NULL COMMENT 'Me relacionara con la tabla de permisos',
  `id_rol` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla de rol',
  `usuario_creo` int(11) DEFAULT NULL COMMENT 'Id del usuario que crea el registro',
  `usuario_mod` int(11) DEFAULT NULL COMMENT 'Id del ultimo usuario en modificar el registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_mod` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que se modifico el registro',
  `estado` varchar(10) DEFAULT NULL COMMENT 'Determina el estado del reistro\r\nActivo\r\nInactivo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Índice 6` (`id_permiso`,`id_rol`,`estado`),
  KEY `FK_rol_asignados` (`id_rol`),
  KEY `FK3_usuario_creador` (`usuario_creo`),
  KEY `FK4_usuario_modifica` (`usuario_mod`),
  CONSTRAINT `FK_permisos_asignados` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_rol_asignados` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='Me relacionara los roles con los permisos asignados';

-- Volcando datos para la tabla grado.permisos_asignados: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos_asignados` DISABLE KEYS */;
INSERT INTO `permisos_asignados` (`id`, `id_permiso`, `id_rol`, `usuario_creo`, `usuario_mod`, `fecha_creacion`, `fecha_mod`, `estado`) VALUES
	(1, 1, 1, 1, 1, '2022-03-13 13:54:18', '2022-03-13 13:54:19', 'Activo'),
	(2, 2, 1, 1, 1, '2022-04-15 15:01:15', '2022-04-15 15:01:16', 'Activo'),
	(3, 3, 1, 1, 1, '2022-04-18 18:16:55', '2022-04-18 18:16:57', 'Activo'),
	(4, 4, 1, 1, 1, '2022-05-01 13:32:47', '2022-05-01 13:32:47', 'Activo'),
	(5, 5, 1, 1, 1, '2022-05-08 11:52:05', '2022-05-08 11:52:06', 'Activo'),
	(6, 3, 2, 1, 1, '2022-05-09 17:38:57', '2022-05-09 17:38:58', 'Activo'),
	(7, 4, 3, 1, 1, '2022-05-09 17:41:02', '2022-05-09 17:41:01', 'Activo'),
	(9, 6, 1, 1, 1, '2022-10-26 21:05:41', '2022-10-26 21:05:42', 'Activo');
/*!40000 ALTER TABLE `permisos_asignados` ENABLE KEYS */;

-- Volcando estructura para tabla grado.proyectos
CREATE TABLE IF NOT EXISTS `proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `titulo` varchar(250) DEFAULT NULL COMMENT 'Titulo del proyecto de grado a validar en el sistema',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que se modifico el dato',
  `user_crea` int(11) DEFAULT NULL COMMENT 'Id del usuario que crea el registro',
  `user_modifica` int(11) DEFAULT NULL COMMENT 'Id del ultimo usuario en modificar el registro',
  `estado` varchar(10) DEFAULT NULL COMMENT 'Estado del registro \r\nActivo\r\nInactivo',
  `descripccion` text DEFAULT NULL COMMENT 'Almacena una lebe descripccion del alcance del proyecto',
  `etapa` varchar(50) DEFAULT NULL COMMENT 'Etapa en la cual se encuentra el proyecto \r\nnull = libre sin asignar \r\nANTE PROYECTO\r\nPROYECTO FINAL\r\nPROYECTO APROBADO',
  PRIMARY KEY (`id`),
  UNIQUE KEY `titulo` (`titulo`),
  KEY `FKuser_crea` (`user_crea`),
  KEY `FK2_user_modifica` (`user_modifica`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='Titulos de los proyectos de grado para su posterior evaluacion y publicación ';

-- Volcando datos para la tabla grado.proyectos: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
INSERT INTO `proyectos` (`id`, `titulo`, `fecha_creacion`, `fecha_modificacion`, `user_crea`, `user_modifica`, `estado`, `descripccion`, `etapa`) VALUES
	(1, 'DESARROLLO DE SOFTWARE SST EN PHP', '2022-04-15 05:40:00', '2022-06-01 07:34:00', 1, 1, 'Activo', NULL, NULL),
	(2, 'SISTEMA DE AUTOMATAS PHP', '2022-05-02 07:37:00', '2022-05-08 04:15:00', 1, 1, 'Activo', NULL, NULL),
	(3, 'DESARROLLO DE SISTEMA E-LEARNING MOVIL', '2022-05-14 05:24:00', NULL, 1, NULL, 'Activo', NULL, NULL),
	(4, '\nSISTEMA DE INFORMACIÓN PARA GENERACION DE AUTOMATAS FD.', '2022-05-18 07:06:00', '2022-05-18 07:06:00', 1, 1, 'Activo', NULL, NULL),
	(5, 'BASE DE DATOS', '2022-06-01 07:41:00', NULL, 1, NULL, 'Activo', NULL, NULL),
	(6, 'APLICATIVO MOVIL E-LEARNING', '2022-06-04 07:00:00', '2022-10-26 08:47:00', 1, 1, 'Activo', NULL, NULL),
	(7, 'SISTEMA WEB DE AUTOMATAS', '2022-10-26 08:27:00', '2022-11-06 17:06:00', 1, 3, 'Activo', 'Plataforma educativa para entender mejor los automatas', 'PROYECTO APROBADO'),
	(8, 'MANUAL ISO 2700 PARA IMPLEMENTACIÓN EN EMPRESAS PRIVADAS', '2022-10-26 08:29:00', '2022-11-06 16:13:00', 1, 3, 'Activo', 'Diseño de manual para que las empresas lo implementen de una manera eficaz', 'ANTE PROYECTO');
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.proyectos_subidos
CREATE TABLE IF NOT EXISTS `proyectos_subidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `user_crea` int(11) DEFAULT NULL COMMENT 'Id del usuario que creo el registro',
  `user_modifica` int(11) DEFAULT NULL COMMENT 'Id del ultimo usuario en modificar el registro',
  `id_proyecto` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla de proyectos para poder determinar el titulo del proyecto que se subio',
  `estado` varchar(50) DEFAULT NULL COMMENT 'Estado del registro \r\nActivo \r\nInactivo',
  `estado_aprobacion` varchar(12) DEFAULT NULL COMMENT 'Me determina el estado de aprobacion del proyecto de grado entregado\r\nAPROBADO\r\nRECHAZADO\r\nPENDIENTE\r\nCORREGIR',
  `id_ante_proyecto_aprobado` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla ante_proyects_subidos para poder determinar el proyecto aprobado para su continuacion de proyecto',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PROYECTO_UNICO` (`estado_aprobacion`,`id_proyecto`),
  KEY `FK_RELACION_PROYECTO` (`id_proyecto`),
  KEY `FK2_USUARIO_CREADOR` (`user_crea`),
  KEY `FK3_USUARIO_MODIFICA` (`user_modifica`),
  KEY `FK2_ANTE_PROYECTO` (`id_ante_proyecto_aprobado`),
  CONSTRAINT `FK2_ANTE_PROYECTO` FOREIGN KEY (`id_ante_proyecto_aprobado`) REFERENCES `ante_proyectos_subidos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_RELACION_PROYECTO` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='Historial de total de proyectos subidos al sistema para su posterior  calificacion y publicacion.\r\n';

-- Volcando datos para la tabla grado.proyectos_subidos: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `proyectos_subidos` DISABLE KEYS */;
INSERT INTO `proyectos_subidos` (`id`, `fecha_creacion`, `fecha_modificacion`, `user_crea`, `user_modifica`, `id_proyecto`, `estado`, `estado_aprobacion`, `id_ante_proyecto_aprobado`) VALUES
	(20, '2022-10-30 05:18:00', '2022-11-07 12:25:00', 1, 3, 7, 'Activo', 'APROBADO', 3);
/*!40000 ALTER TABLE `proyectos_subidos` ENABLE KEYS */;

-- Volcando estructura para tabla grado.rol
CREATE TABLE IF NOT EXISTS `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre del rol ej\r\nADMINISTRADOR\r\nESTUDIANTE\r\nDOCENTE',
  `usuario_creo` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla de usuario \r\npara poder determinar el usuario que creo el registro',
  `usuario_mod` int(11) DEFAULT NULL COMMENT 'Relacion con la tabla de usuario \r\npara poder determinar el ultimo usuario en modificar el registro',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se creo el registro',
  `fecha_mod` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en que fue modificado el registro',
  `estado` varchar(10) DEFAULT NULL COMMENT 'Determina el estado del registro\r\nActivo\r\nInactivo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rol` (`nombre`),
  KEY `FK_usuario_crea` (`usuario_creo`),
  KEY `FK_usuario_modifica` (`usuario_mod`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='Nivel de acceso que se determinara para los usuarios';

-- Volcando datos para la tabla grado.rol: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`id`, `nombre`, `usuario_creo`, `usuario_mod`, `fecha_creacion`, `fecha_mod`, `estado`) VALUES
	(1, 'ADMINISTRADOR', 1, 1, '2022-03-13 13:52:52', '2022-03-13 13:52:53', 'Activo'),
	(2, 'ESTUDIANTE', 1, 1, '2022-04-15 15:25:30', '2022-04-15 15:25:30', 'Activo'),
	(3, 'DIRECTOR ', 1, 1, '2022-05-08 16:19:21', '2022-05-08 16:19:23', 'Activo');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;

-- Volcando estructura para tabla grado.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria e identificador unico de registro',
  `user` varchar(50) DEFAULT NULL COMMENT 'Numero de identificacion del usuario EJEMPLO numero de cedula',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Primer y segundo nombre del usuario con acceso al sistema',
  `apellidos` varchar(100) DEFAULT NULL COMMENT 'Primer y segundo apellido del usuario',
  `estado` varchar(10) DEFAULT NULL COMMENT 'Estado del usuario\r\nActivo\r\nInactivo',
  `fecha_creacion` timestamp NULL DEFAULT NULL COMMENT 'Fecha en que se crea el registro',
  `fecha_modificacion` timestamp NULL DEFAULT NULL COMMENT 'Ultima fecha en ser modificado el registro',
  `id_rol` int(11) DEFAULT NULL COMMENT 'Me relaciona con la tabla rol para determinar el nivel de acceso del usuario',
  `password` varchar(80) DEFAULT NULL COMMENT 'Contraseña del usuario encryptada para acceder al sistema',
  `email` varchar(50) DEFAULT NULL COMMENT 'Correo electronico del usuario para poder realizar un restablecimiento de contraseñas',
  `identificacion` varchar(15) DEFAULT NULL COMMENT 'Numero de identificacion del usuario',
  `cambio_password` varchar(2) DEFAULT NULL COMMENT 'Determina si restablecio la contraseña y se necesita realizar cambio de contraseña\r\nSI = se debe pedir cambio de contraseña\r\nNULL = no se pide ningun cambio y el proceso continua con normalidad',
  `director_project` varchar(2) DEFAULT NULL COMMENT 'En caso de que el usuario pueda ser asignado como director de proyecto se almacenara la palabra SI de lo contrario permanecera como null',
  `evaluador_project` varchar(2) DEFAULT NULL COMMENT 'En caso de que el usuario pueda ser asignado como evaluador de proyectos se almacenara la palabra SI de lo contrario permanecera como null',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Índice 3` (`identificacion`,`user`),
  KEY `FK1_rol` (`id_rol`),
  CONSTRAINT `FK1_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='Tabla donde se almacenara el listado de usuarios con acceso al sistema';

-- Volcando datos para la tabla grado.usuario: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `user`, `nombre`, `apellidos`, `estado`, `fecha_creacion`, `fecha_modificacion`, `id_rol`, `password`, `email`, `identificacion`, `cambio_password`, `director_project`, `evaluador_project`) VALUES
	(1, '1095825459', 'BRANM ALDAIR', 'PABON VILLAMIZAR', 'Activo', '2022-03-13 13:52:20', '2022-04-15 03:41:00', 1, '$2a$10$GvhnF/9x9KYJOlij6y41B.B7zq6pjSZMzhmPg7lXUHG8kKDlddDYS', 'bpabon1@udi.edu.co', '1095825459', NULL, NULL, 'SI'),
	(2, '28334960', 'AMILDE', 'VILLAMIZAR CACERES', 'Activo', '2022-04-14 05:49:00', '2022-10-24 09:19:00', 2, '$2a$10$IlP3XehlDsPUqqRg2RT4WugSNf77S0RiMmdcRvbpdCLoEIKiVxtiK', 'sendmailfree.project@gmail.com', '28334960', NULL, NULL, NULL),
	(3, '109567434', 'LUIS,', 'DIAS ', 'Activo', '2022-04-14 06:00:00', '2022-10-26 06:15:00', 1, '$2a$10$DOm4p8B5q7qVmz.QuXKSx.fooefE3XdrsS.jk5h0745QuyQHVpjVa', 'ldiaz@gmail.com', '109567434', NULL, NULL, NULL),
	(4, '1097656789', 'ARNULFO', 'PABON PEREZ', 'Activo', '2022-05-02 07:35:00', NULL, 2, '$2a$10$ohdX9ru8g3n7iwdFgJus3errU3813IdDSE.usZbbTr5PjNWI6iPm2', 'sanchez3@gmail.com', '1097656789', NULL, NULL, NULL),
	(5, '1019654324', 'REYNOL', 'CARRILLO ', 'Activo', '2022-05-08 04:13:00', '2022-10-29 10:49:00', 3, '$2a$10$ke3EyhVuulD9Y9j.pJ6QJOEd65VskV5AqBegOpWa.uvuQyYNEUAEG', 'rcarrillo@udi.edu.co', '1019654324', NULL, 'SI', 'SI'),
	(6, '27665432', 'CAMILO', 'PEREZ ROJAS', 'Activo', '2022-05-08 04:15:00', NULL, 2, '$2a$10$Gzar.oDUP5YjyLL9SgB4JelTk./OvoJKq2dHdGPKj4nxxubUwN0k2', 'cperez@udi.edu.co', '27665432', NULL, NULL, NULL),
	(7, '109654378', 'CLAUDIA', 'LOPEZ', 'Activo', '2022-05-08 04:16:00', NULL, 2, '$2a$10$fFwD1ptTiA32Z1zH8frO/OlnLbvOSg6qe8V24nRF/Oz1mwleIt0wu', 'clopez@udi.edu.co', '109654378', NULL, NULL, NULL),
	(8, '1097654245', 'MONICA', 'SANCHEZ SEQUEDA', 'Activo', '2022-05-08 04:20:00', '2022-10-26 06:17:00', 3, '$2a$10$cc73sZE/B7TvqO2vVPGWQOUYJ8B1r5WMafwE9VlGy7nwl9SHUz40O', 'msanchez@udi.edu.co', '1097654245', NULL, 'SI', 'SI'),
	(9, '1096745388', 'JUAN', 'PEREZ SANCHE', 'Activo', '2022-05-18 06:48:00', '2022-05-18 06:49:00', 2, '$2a$10$wMXsxq.DztumcpgnHr3X7euPbPatNXkRVnbIUEOD8wgZ7KonYK2/a', 'PRUEBA@GMAIL.COM', '1096745388', NULL, NULL, NULL),
	(10, '123456789', 'ALDAIR B', 'PABON VILLAMIZAR', 'Activo', '2022-06-01 07:40:00', NULL, 2, '$2a$10$Y9M/aNZ9iwaEnhGubR1NyOHksxU//Zl9.RHwYHB2VLUFTRkmPFYi.', 'aldasoftw@gmail.com', '123456789', NULL, NULL, NULL),
	(11, '976323721', 'CARLOS', 'SANCHEZ SEQUEDA', 'Activo', '2022-06-04 07:00:00', '2022-11-06 02:05:00', 3, '$2a$10$567v6hhQGbVgprQLAmXp5Ol69nm4F/uk7IfE9Cdy6v8bHoTajaBmK', 'carlos123@gmail.com', '976323721', NULL, 'SI', 'SI'),
	(12, '10958765435', 'GERSON', 'ORTIZ PEREZ', 'Activo', '2022-10-26 06:04:00', '2022-10-29 10:49:00', 3, '$2a$10$YMX9bjjWLVa3B1rKzOhn7O2k7fuLifqI/IHslWEz1X0uaj3OjqJtW', 'cortiz@gmail.com', '10958765435', NULL, 'SI', 'SI');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
