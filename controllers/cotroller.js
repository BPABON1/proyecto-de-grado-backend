const { request, response } = require("express");
const { compareSync, hashSync, genSaltSync } = require("bcryptjs");
const { httpError } = require("../helpers/handleError");
const { generarJWT } = require("../helpers/jasonwebtoken");
const moment = require("moment");
const { Op } = require("sequelize");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const { networkInterfaces } = require("os");
const { sendMail } = require("../helpers/SendMailer");
const {
  Usuarios,
  Roles,
  PermisosAsignados,
  Permisos,
  Projects,
  StudentProjects,
  ProjectsUpload,
  DocumentosSubidos,
  EvaluadoresProyectos,
} = require("../models/");
Roles.hasMany(Usuarios, { foreignKey: "id_rol" });
Projects.hasMany(StudentProjects, { foreignKey: "id_proyecto" });
Roles.hasMany(PermisosAsignados, { foreignKey: "id_rol" });
PermisosAsignados.belongsTo(Permisos, { foreignKey: "id_permiso" });
Usuarios.belongsTo(Roles, { foreignKey: "id_rol" });
StudentProjects.belongsTo(Usuarios, { foreignKey: "id_estudiante" });
ProjectsUpload.hasMany(DocumentosSubidos, { foreignKey: "id_proyecto_subido" });
Projects.hasOne(ProjectsUpload, { foreignKey: "id_proyecto" });
Projects.hasMany(EvaluadoresProyectos, { foreignKey: "id_proyecto", as:'DocentesAnteProyecto' });
EvaluadoresProyectos.belongsTo(Usuarios, { foreignKey: "id_director",as:'DirectorAnteProyecto' });
EvaluadoresProyectos.belongsTo(Usuarios, { foreignKey: "id_evaluador",as:'EvaluadorAnteProyecto' });

// Cargar el total de proyectos creados en el sistema y listar el total de estudiantes
const ListProjectAsignar = async (req, res) => {
    try {
        const TotalProjectsEtapas = await Projects.findAll({
            include: [
              {
                model: StudentProjects,
                required: true,
                include: [
                  {
                    model: Usuarios,
                    required: true,
                  },
                ],
              },
              { 
                model: EvaluadoresProyectos,
                required: true,
                as: 'DocentesAnteProyecto',
                include: [
                    {
                      model: Usuarios,
                      as: 'DirectorAnteProyecto',
                      required: false,
                    },
                    {
                        model: Usuarios,
                        as: 'EvaluadorAnteProyecto',
                        required: false,
                      },
                  ],
              },
            ],
            where: { etapa: ['ANTE PROYECTO','PROYECTO FINAL']}
          });
        // Total usuarios estudiantes activos con sin permisos de evaluar ni del director de proyecto
        const TotalUsuariosEstudiantesActivos = await Usuarios.findAll({
        include: [
          {
            model: Roles,
            required: true,
            where: { estado: "Activo" },
          },
        ],
        where: { estado: "Activo",director_project: null,evaluador_project: null },
      });
      // Total proyectos activos 
      const TotalProjects = await Projects.findAll({
        where: {
            estado: 'Activo',
        }
      });
      // Total proyectos asignados a proyectos 
      const TotalProjectsAsignados = await Projects.findAll({
        include: [
          {
            model: StudentProjects,
            required: true,
            include: [
              {
                model: Usuarios,
                required: true,
              },
            ],
          },
        ]
      });
      // Total usuarios con permisos de ser directores de proyectos 
      const TotalDirectoresActivos = await Usuarios.findAll({
        include: [
          {
            model: Roles,
            required: true,
            where: { estado: "Activo" },
          },
        ],
        where: { estado: "Activo",director_project: {[Op.ne]: null} },
      });
      // Total de usuarios con permisos de evaluadores activos 
      const TotalEvaluadoresActivos = await Usuarios.findAll({
        include: [
          {
            model: Roles,
            required: true,
            where: { estado: "Activo" },
          },
        ],
        where: { estado: "Activo",evaluador_project: {[Op.ne]: null} },
      });
      let TotalProyectosLibres = [];
      for(let item of TotalProjects){
        var existe = 'NO';
        for(let value of TotalProjectsAsignados){
            if(value.id === item.id){
                existe = 'SI';
            }
        }
        if(existe === 'NO'){
            TotalProyectosLibres.push(item);
        }
      }
      res.status(200).json({
        projects: TotalProjects,
        ProjectsAsignados: TotalProjectsAsignados,
        TotalEstudiantesActivos: TotalUsuariosEstudiantesActivos,
        TotalDirectoresActivos: TotalDirectoresActivos,
        TotalEvaluadoresActivos: TotalEvaluadoresActivos,
        TotalProyectosLibres: TotalProyectosLibres,
        EtapasProyecto: TotalProjectsEtapas
      });
    } catch (error) {
      res.status(404).json({
        msg: error.message,
      });
    }
  };
// Cargar el total de proyectos creados en el sistema y listar el total de estudiantes
const SaveAnteProjects = async (req, res) => {
    try {
        let fecha = moment().format("YYYY-MM-DD HH:mm");
        if(req.body.titulo.length > 0) {
            if(req.body.director.length > 0){
                if(req.body.evaluador.length > 0){
                    await Promise.all(req.body.estudiantes.map(async (value) => {
                        await StudentProjects.create({
                            id_proyecto: req.body.titulo[0].id,
                            id_estudiante: value.id,
                            fecha_creacion: fecha,
                            user_crea:req.usuario.id,
                        });
                    })).then((succes) =>{
                        Promise.all(req.body.evaluador.map(async (data) => {
                            await EvaluadoresProyectos.create({
                                id_proyecto: req.body.titulo[0].id,
                                id_evaluador: data.id,
                                fecha_creacion: fecha,
                                user_crea:req.usuario.id,
                                tipo: 'ANTE PROYECTO',
                                id_director: req.body.director[0].id,
                            });
                            await Projects.update({
                                etapa: 'ANTE PROYECTO',
                                fecha_modificacion: fecha,
                                user_modifica: req.usuario.id,
                            },{where: {id: req.body.titulo[0].id}})
                        })).then(correcto =>{
                            res.status(200).json({
                                msg: 'Asignación de ante proyecto creada de forma correcta',
                            });
                        }).catch(errores =>{
                            res.status(404).json({
                                msg: 'Ocurrio un problema al asociar los evaluadores',
                            });
                        });
                    }).catch((err) => {
                        res.status(404).json({
                            msg: 'Ocurrio un problema al asociar los estudiantes al proyecto',
                        });
                    });
                }else{
                    res.status(404).json({
                        msg: 'Se debe relacionar almenos un evaluador de proyecto',
                    });
                }
            }else{
                res.status(404).json({
                    msg: 'Se debe relacionar un director de proyecto',
                });
            }
        }else{
            res.status(404).json({
                msg: 'Se debe relacionar un proyecto',
            });
        }
    } catch (error) {
        res.status(404).json({
          msg: error.message,
        });
      }
    };
// Eliminar el estudiante seleccionado del ante proyecto o proyecto de grado
const DeleteEstudianteAnteProyecto = async (req, res) => {
    try {
        await StudentProjects.destroy({
            where: {
                id: req.body.id_relacional,
            }
        }).then(succes => {
            res.status(200).json({
                msg: 'Estudiante eliminado de forma correcta',
              });
        }).catch(err => {
            res.status(404).json({
                msg: err.message,
              });
        });
    } catch (error) {
        res.status(404).json({
          msg: error.message,
        });
      }
    };
    // Eliminar el estudiante seleccionado del ante proyecto o proyecto de grado
const DeleteDirecotrAnteProyecto = async (req, res) => {
    try {
        await EvaluadoresProyectos.update({
            id_director: null,
        },{where:{tipo:'ANTE PROYECTO',id_proyecto: req.body.id_relacional}}).then(succes => {
            res.status(200).json({
                msg: 'Director eliminado de forma correcta',
              });
        }).catch(err => {
            res.status(404).json({
                msg: err.message,
              });
        });
    } catch (error) {
        res.status(404).json({
          msg: error.message,
        });
      }
    };
// Eliminar el evaluador seleccionado del ante proyecto o proyecto de grado
const DeleteEvaluadorAnteProyecto = async (req, res) => {
    try {
        await EvaluadoresProyectos.destroy({
            where: {
                id: req.body.id_relacional
            }
        }).then(succes => {
            res.status(200).json({
                msg: 'Evaluador eliminado de forma correcta',
              });
        }).catch(err => {
            res.status(404).json({
                msg: err.message,
              });
        });
    } catch (error) {
        res.status(404).json({
          msg: error.message,
        });
      }
    };
    // Cargar el total de proyectos creados en el sistema y listar el total de estudiantes
const UpdateAnteProjects = async (req, res) => {
try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    if(req.body.director.length > 0){
        if(req.body.evaluador.length > 0){
            await Promise.all(req.body.estudiantes.map(async (value) => {
                if(value.bandera === 'NUEVO'){
                    await StudentProjects.create({
                        id_proyecto: req.body.titulo,
                        id_estudiante: value.id,
                        fecha_creacion: fecha,
                        user_crea:req.usuario.id,
                    });
                }
            })).then((succes) =>{
                Promise.all(req.body.evaluador.map(async (data) => {
                    if(data.bandera === 'NUEVO'){
                        await EvaluadoresProyectos.create({
                            id_proyecto: req.body.titulo,
                            id_evaluador: data.id,
                            fecha_creacion: fecha,
                            user_crea:req.usuario.id,
                            tipo: 'ANTE PROYECTO',
                            id_director: req.body.director[0].id,
                        });
                    }
                })).then(correcto =>{
                    EvaluadoresProyectos.update({
                        id_director: req.body.director[0].id,
                    },{where: {id_proyecto: req.body.titulo}});
                    Projects.update({
                        estado: req.body.estado,
                        fecha_modificacion: fecha,
                        user_modifica: req.usuario.id,
                    },{where: {id: req.body.titulo}});
                    res.status(200).json({
                        msg: 'Asignación de ante proyecto actualizada de forma correcta',
                    });
                }).catch(errores =>{
                    console.log(errores);
                    res.status(404).json({
                        msg: 'Ocurrio un problema al asociar los evaluadores',
                    });
                });
            }).catch((err) => {
                res.status(404).json({
                    msg: 'Ocurrio un problema al asociar los estudiantes al proyecto',
                });
            });
        }else{
            res.status(404).json({
                msg: 'Se debe relacionar almenos un evaluador de proyecto',
            });
        }
    }else{
        res.status(404).json({
            msg: 'Se debe relacionar un director de proyecto',
        });
    }
} catch (error) {
    res.status(404).json({
        msg: error.message,
    });
    }
};
// Eliminar el director seleccionado del proyecto de grado
const DeleteDirecotrProyectoFinal = async (req, res) => {
    try {
        await EvaluadoresProyectos.update({
            id_director: null,
        },{where:{tipo: 'PROYECTO FINAL',id_proyecto: req.body.id_relacional }}).then(succes => {
            res.status(200).json({
                msg: 'Director eliminado de forma correcta',
                });
        }).catch(err => {
            res.status(404).json({
                msg: err.message,
                });
        });
    } catch (error) {
        res.status(404).json({
            msg: error.message,
        });
        }
    };
    // Eliminar el evaluador seleccionado del proyecto de grado
const DeleteEvaluadorProyectoFinal = async (req, res) => {
    try {
        await EvaluadoresProyectos.destroy({
            where: {
                id: req.body.id_relacional
            }
        }).then(succes => {
            res.status(200).json({
                msg: 'Evaluador eliminado de forma correcta',
              });
        }).catch(err => {
            res.status(404).json({
                msg: err.message,
              });
        });
    } catch (error) {
        res.status(404).json({
          msg: error.message,
        });
      }
    };
// Actualizar los datos del proyecto de grado final
const UpdateProjectsFinal = async (req, res) => {
    try {
        let fecha = moment().format("YYYY-MM-DD HH:mm");
        if(req.body.director.length > 0){
            if(req.body.evaluador.length > 0){
                await Promise.all(req.body.estudiantes.map(async (value) => {
                    if(value.bandera === 'NUEVO'){
                        await StudentProjects.create({
                            id_proyecto: req.body.titulo,
                            id_estudiante: value.id,
                            fecha_creacion: fecha,
                            user_crea:req.usuario.id,
                        });
                    }
                })).then((succes) =>{
                    Promise.all(req.body.evaluador.map(async (data) => {
                        if(data.bandera === 'NUEVO'){
                            await EvaluadoresProyectos.create({
                                id_proyecto: req.body.titulo,
                                id_evaluador: data.id,
                                fecha_creacion: fecha,
                                user_crea:req.usuario.id,
                                tipo: 'PROYECTO FINAL',
                                id_director: req.body.director[0].id,
                            });
                        }
                    })).then(correcto =>{
                        EvaluadoresProyectos.update({
                            id_director: req.body.director[0].id,
                        },{where: {id_proyecto: req.body.titulo, tipo: 'PROYECTO FINAL'}});
                        Projects.update({
                            estado: req.body.estado,
                            fecha_modificacion: fecha,
                            user_modifica: req.usuario.id,
                        },{where: {id: req.body.titulo}});
                        res.status(200).json({
                            msg: 'Asignación de proyecto actualizada de forma correcta',
                        });
                    }).catch(errores =>{
                        console.log(errores);
                        res.status(404).json({
                            msg: 'Ocurrio un problema al asociar los evaluadores',
                        });
                    });
                }).catch((err) => {
                    res.status(404).json({
                        msg: 'Ocurrio un problema al asociar los estudiantes al proyecto',
                    });
                });
            }else{
                res.status(404).json({
                    msg: 'Se debe relacionar almenos un evaluador de proyecto',
                });
            }
        }else{
            res.status(404).json({
                msg: 'Se debe relacionar un director de proyecto',
            });
        }
    } catch (error) {
        res.status(404).json({
            msg: error.message,
        });
        }
    };
module.exports = {
    ListProjectAsignar,
    SaveAnteProjects,
    DeleteEstudianteAnteProyecto,
    DeleteDirecotrAnteProyecto,
    DeleteEvaluadorAnteProyecto,
    UpdateAnteProjects,
    DeleteDirecotrProyectoFinal,
    DeleteEvaluadorProyectoFinal,
    UpdateProjectsFinal,
  };