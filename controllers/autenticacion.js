const { request, response } = require("express");
const { compareSync, hashSync, genSaltSync } = require("bcryptjs");
const { httpError } = require("../helpers/handleError");
const { generarJWT } = require("../helpers/jasonwebtoken");
const moment = require("moment");
const { Op } = require("sequelize");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const { networkInterfaces } = require("os");
const { sendMail } = require("../helpers/SendMailer");
const {
  Usuarios,
  Roles,
  PermisosAsignados,
  Permisos,
  Projects,
  StudentProjects,
  ProjectsUpload,
  DocumentosSubidos,
  AnteProjectsUpload,
  NotasAnteProyecto,
  NotasProyecto,
  EvaluadoresProyectos,
} = require("../models/");
Roles.hasMany(Usuarios, { foreignKey: "id_rol" });
Projects.hasMany(StudentProjects, { foreignKey: "id_proyecto" });
Roles.hasMany(PermisosAsignados, { foreignKey: "id_rol" });
PermisosAsignados.belongsTo(Permisos, { foreignKey: "id_permiso" });
Usuarios.belongsTo(Roles, { foreignKey: "id_rol" });
StudentProjects.belongsTo(Usuarios, { foreignKey: "id_estudiante" });
ProjectsUpload.hasMany(DocumentosSubidos, { foreignKey: "id_proyecto_subido" });
Projects.hasOne(ProjectsUpload, { foreignKey: "id_proyecto" });
AnteProjectsUpload.hasMany(DocumentosSubidos, {
  foreignKey: "id_ante_proyecto",
  as: "DocumentoAnteProyecto",
});
ProjectsUpload.hasMany(DocumentosSubidos, {
  foreignKey: "id_proyecto_subido",
  as: "DocumentoProyectoFinal",
});
ProjectsUpload.hasMany(DocumentosSubidos, {
  foreignKey: "id_proyecto_subido",
  as: "DocumentoProyectoProyectoFinal",
});
DocumentosSubidos.hasMany(NotasAnteProyecto, {
  foreignKey: "id_documento",
  as: "NotasAnteProyectos",
});
DocumentosSubidos.hasMany(NotasProyecto, {
  foreignKey: "id_documento",
  as: "NotasProyectosFinal",
});
Projects.hasOne(AnteProjectsUpload, {
  foreignKey: "id_proyecto",
  as: "AnteProyectosSubidos",
});
Projects.hasMany(EvaluadoresProyectos, {
  foreignKey: "id_proyecto",
  as: "EvaluadoresAnteProyecto",
});
EvaluadoresProyectos.belongsTo(Usuarios, { foreignKey: "user_crea",as: "NameEvaluadores", });
EvaluadoresProyectos.belongsTo(Usuarios, { foreignKey: "id_director",as: "NameDirector", });
Projects.hasOne(ProjectsUpload, {
  foreignKey: "id_proyecto",
  as: "ProyectosFinalesSubidos",
});
// Validar las  credenciales de acceso de inisio de sesion del usuario
const IniciarSesion = async (req = request, res = response) => {
  try {
    const user = await Usuarios.findOne({
      attribute: [
        "id",
        "nombre",
        "apellidos",
        "user",
        "email",
        "identificacion",
        "",
      ],
      where: { user: req.body.user, estado: "Activo" },
      include: [
        {
          model: Roles,
          required: true,
        },
      ],
    });
    if (user !== null) {
      if (compareSync(req.body.password, user.password)) {
        if (user.cambio_password !== "SI") {
          const permisos = await PermisosAsignados.findAll({
            include: [
              {
                model: Permisos,
                required: true,
              },
            ],
            where: { id_rol: user.Role.id },
          });
          if (permisos.length > 0) {
            const token = await generarJWT(user.id);
            res.status(200).json({
              msg: "Bienvenido al sistema",
              user: user,
              permisos: permisos,
              token: token,
            });
          } else {
            res.status(404).json({
              msg: "Este usuario no cuenta con permisos para ingresar al sistema",
            });
          }
        } else {
          res.status(404).json({
            msg: "Se requiere cambio de contraseña.",
            data: user,
          });
        }
      } else {
        res.status(404).json({
          msg: "La contraseña suministrada es incorrecta",
        });
      }
    } else {
      res.status(404).json({
        msg: "Este usuario no existe en el sistema por favor verifique la información",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Mostrar la lista total de usuarios en el sistema
const ListUserRoles = async (req, res) => {
  try {
    const totalRoles = await Roles.findAll({
      where: { estado: "Activo" },
    });
    const totalUsuarios = await Usuarios.findAll({
      include: [
        {
          model: Roles,
          required: true,
        },
      ],
    });
    if (totalRoles.length > 0) {
      res.status(200).json({
        roles: totalRoles,
        usuarios: totalUsuarios,
      });
    } else {
      res.status(404).json({
        msg: "No existen roles en el sistema por favor defina primero los roles",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Guardar el usuario nuevo en el sistema
const SaveUsuer = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    let PassworEncryptada = hashSync(req.body.password, genSaltSync(10));
    await Usuarios.create({
      user: req.body.identificacion,
      nombre: req.body.nombres,
      apellidos: req.body.apellidos,
      estado: "Activo",
      fecha_creacion: fecha,
      id_rol: req.body.id_rol,
      password: PassworEncryptada,
      email: req.body.email,
      identificacion: req.body.identificacion,
      director_project: req.body.is_director,
      evaluador_project: req.body.is_evaluador,
    })
      .then((succes) => {
        res.status(200).json({
          msg: "Usuario creado de forma correcta",
        });
      })
      .catch((err) => {
        res.status(404).json({
          msg: err.message,
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Actualizar el usuario seleccionado
const UpdateUsuer = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    if (req.body.password === null) {
      await Usuarios.update(
        {
          nombre: req.body.nombres,
          apellidos: req.body.apellidos,
          estado: req.body.estado,
          fecha_modificacion: fecha,
          id_rol: req.body.id_rol,
          email: req.body.email,
          director_project: req.body.is_director,
          evaluador_project: req.body.is_evaluador,
        },
        { where: { id: req.body.id } }
      )
        .then((succes) => {
          res.status(200).json({
            msg: "Usuario actualizado de forma correcta",
          });
        })
        .catch((err) => {
          res.status(404).json({
            msg: err.message,
          });
        });
    } else {
      let PassworEncryptada = hashSync(req.body.password, genSaltSync(10));
      await Usuarios.update(
        {
          nombre: req.body.nombres,
          apellidos: req.body.apellidos,
          estado: req.body.estado,
          fecha_modificacion: fecha,
          id_rol: req.body.id_rol,
          password: PassworEncryptada,
          email: req.body.email,
        },
        { where: { id: req.body.id } }
      )
        .then((succes) => {
          res.status(200).json({
            msg: "Usuario actualizado de forma correcta",
          });
        })
        .catch((err) => {
          res.status(404).json({
            msg: err.message,
          });
        });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Cargar el total de proyectos creados en el sistema y listar el total de estudiantes
const ListStudentProjects = async (req, res) => {
  try {
    /* const totalUsuarios = await Usuarios.findAll({
      include: [
        {
          model: Roles,
          required: true,
          where: { nombre: "ESTUDIANTE", estado: "Activo" },
        },
      ],
      where: { estado: "Activo" },
    }); */
    const TotalProjects = await Projects.findAll({
      /* include: [
        {
          model: StudentProjects,
          required: true,
          include: [
            {
              model: Usuarios,
              required: true,
            },
          ],
        },
      ], */
    });
    res.status(200).json({
      // usuarios: totalUsuarios,
      projects: TotalProjects,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Guardar el usuario nuevo en el sistema
const SaveProjects = async (req, res) => {
  try {
    // let EstudianteRegistrado = "NO";
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    /* Promise.all(
      req.body.estudiantes.map(async (item) => {
        let Estudiante = await StudentProjects.findOne({
          where: { id_estudiante: item.id },
        });
        if (Estudiante !== null) {
          EstudianteRegistrado =
            "El estudiante " +
            item.nombre_completo +
            " ya se encuentra asignado a otro proyecto de grado";
        }
      })
    ).finally(async function (succes) {
      if (EstudianteRegistrado !== "NO") {
        res.status(404).json({
          msg: EstudianteRegistrado,
        });
      } else { */
    await Projects.create({
      titulo: req.body.titulo,
      fecha_creacion: fecha,
      user_crea: req.usuario.id,
      estado: "Activo",
      descripccion: req.body.descripcion,
    })
      .then(async function (info) {
        /*Promise.all(
              req.body.estudiantes.map(async (estudent) => {
                await StudentProjects.create({
                  id_proyecto: info.id,
                  id_estudiante: estudent.id,
                  fecha_creacion: fecha,
                  fecha_modificacion: fecha,
                  user_crea: req.usuario.id,
                  user_modifica: req.usuario.id,
                });
              })
            ); */
        res.status(200).json({
          msg: "Proyecto creado y asociado de forma correcta",
        });
      })
      .catch((err) => {
        res.status(404).json({
          msg: err.message,
        });
      });
    // }
    // });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Guardar el usuario nuevo en el sistema
const UpdateProjects = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    await Projects.update(
      {
        titulo: req.body.titulo,
        fecha_modificacion: fecha,
        user_modifica: req.usuario.id,
        estado: req.body.estado,
        descripccion: req.body.descripcion,
      },
      {
        where: { id: req.body.id },
      }
    )
      .then(async function (info) {
        res.status(200).json({
          msg: "Proyecto actualizado de forma correcta",
        });
      })
      .catch((erro) => {
        res.status(404).json({
          msg: "Ocurrió un problema al actualizar el proyecto de grado ",
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Eliminar el usuario de la asociacion del proyecto
const DeleteProjects = async (req, res) => {
  try {
    await StudentProjects.destroy({
      where: {
        id: req.body.id_relacion,
      },
    })
      .then((succes) => {
        res.status(200).json({
          msg: "Datos actualizados de forma correcta",
        });
      })
      .catch((err) => {
        res.status(404).json({
          msg: " Ocurrió un problema al eliminar el estudiante del proyecto de grado",
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Detalle de los proyectos subidos y pendientes subidos para
const DetalleProjectsPersonal = async (req, res) => {
  try {
    /* 
    const nets = networkInterfaces();
    const results = Object.create(null);

    for (const name of Object.keys(nets)) {
      for (const net of nets[name]) {
        if (net.family === "IPv4" && !net.internal) {
          if (!results[name]) {
            results[name] = [];
          }
          results[name].push(net.address);
        }
      }
    }
    const search = 'C:/xampp/htdocs';  
    const replacer = new RegExp(search, 'g');
    console.log(results['Wi-Fi']); 
    var temporal = 'C:/xampp/htdocs/grado/proyecto-de-grado-backend/uploads/cec7c268-720b-4327-a921-0a17fa09e867.pdf';
    let NuevaRuta = temporal.replace(replacer, 'http://'+results['Wi-Fi']);
    console.log(NuevaRuta);  */
    // const ExisteSubir = await
    const InfoProject = await Projects.findOne({
      include: [
        {
          model: StudentProjects,
          required: true,
          where: { id_estudiante: req.usuario.id },
        },
      ],
    });
    if (InfoProject !== null) {
      const DetalleProyecto = await ProjectsUpload.findOne({
        include: [
          {
            model: DocumentosSubidos,
            required: true,
          },
        ],
        where: { estado: "Activo", id_proyecto: InfoProject.id },
      });
      res.status(200).json({
        historial: DetalleProyecto,
        info: InfoProject,
      });
    } else {
      res.status(404).json({
        msg: "El estudiante no cuenta con proyectos asignados para subir",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Crear el documento y subirlo a la base de datos y crear el acceso para posteriormete ser consultado
const UploadProjects = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    await CreateDocumento(req, res)
      .then(async function (succes) {
        const ExisteProyecto = await ProjectsUpload.findOne({
          where: { id_proyecto: req.params.idproject },
        });
        if (ExisteProyecto !== null) {
          await ProjectsUpload.update(
            {
              fecha_modificacion: fecha,
              user_modifica: req.usuario.id,
              estado_aprobacion: "PENDIENTE",
            },
            { where: { id: ExisteProyecto.id } }
          )
            .then(async function (correcto) {
              let reqPath = path.join(__dirname, "../uploads/");
              let ruta = reqPath + succes;
              await DocumentosSubidos.create({
                fecha_creacion: fecha,
                //fecha_modificacion: fecha,
                user_entrego: req.usuario.id,
                url_documento: ruta,
                id_proyecto_subido: ExisteProyecto.id,
                resumen: req.params.resumen,
                // conclusion_publica: req.params.concluciones,
              })
                .then((exito) => {
                  res.status(200).json({
                    msg: "Documento subido de forma exitosa",
                    bandera: "BIEN",
                  });
                })
                .catch((errores) => {
                  res.status(404).json({
                    msg: " Ocurrió un problema al relacionar el proyecto",
                    bandera: "MAL",
                    info: error,
                  });
                });
            })
            .catch((err) => {
              res.status(404).json({
                msg: " Ocurrió un problema al editar el registro del proyecto",
                bandera: "MAL",
                info: err,
              });
            });
        } else {
          await ProjectsUpload.create({
            fecha_creacion: fecha,
            user_crea: req.usuario.id,
            id_proyecto: req.params.idproject,
            estado: "Activo",
            estado_aprobacion: "PENDIENTE",
          })
            .then(async function (datos) {
              const idProject = datos.id;
              let reqPath = path.join(__dirname, "../uploads/");
              let ruta = reqPath + succes;
              await DocumentosSubidos.create({
                fecha_creacion: fecha,
                //fecha_modificacion: fecha,
                user_entrego: req.usuario.id,
                url_documento: ruta,
                id_proyecto_subido: idProject,
                resumen: req.params.resumen,
                // conclusion_publica: req.params.concluciones,
              })
                .then((exito) => {
                  res.status(200).json({
                    msg: "Documento subido de forma exitosa",
                    bandera: "BIEN",
                  });
                })
                .catch((errores) => {
                  res.status(404).json({
                    msg: " Ocurrió un problema al relacionar el proyecto",
                    bandera: "MAL",
                    info: error,
                  });
                });
            })
            .catch((error) => {
              res.status(404).json({
                msg: " Ocurrió un problema al crear el registro del proyecto",
                bandera: "MAL",
                info: error,
              });
            });
        }
      })
      .catch((error) => {
        res.status(404).json({
          msg: " Ocurrió un problema inesperado",
          bandera: "MAL",
          info: error,
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
/*
 * proposito:Crear el Documento subido y almacearlo en la base de datos
 */
function CreateDocumento(req, res) {
  return new Promise(async function (resolve, reject) {
    try {
      // console.log(documento)
      const NameDocumento = req.files.archivo.name.split(".");
      const extension = NameDocumento[NameDocumento.length - 1];
      if (extension === "pdf") {
        const nombreTemp = uuidv4() + "." + extension;
        const uploadPath = path.join(__dirname, "../uploads/", nombreTemp);
        req.files.archivo.mv(uploadPath, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve(nombreTemp);
          }
        });
      } else {
        reject("La extesión no es permitida");
      }
    } catch (err) {
      reject(err.message);
    }
  });
}
// Listar tesis de grado pendientes por evaluar
const ListProjectEvaluar = async (req, res) => {
  try {
    const Lista = await Projects.findAll({
      include: [
        {
          model: StudentProjects,
          required: true,
          include: [
            {
              model: Usuarios,
              required: true,
            },
          ],
        },
        {
          model: ProjectsUpload,
          required: true,
          where: { estado_aprobacion: "PENDIENTE" },
        },
      ],
    });
    res.status(200).json({
      lista: Lista,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Buscar la información detallada del proyecto seleccionado
const DetalleProjectEvaluar = async (req, res) => {
  try {
    const InfoProject = await Projects.findOne({
      include: [
        {
          model: StudentProjects,
          required: true,
        },
      ],
      where: { estado: "Activo", id: req.body.id_proyecto },
    });
    if (InfoProject !== null) {
      const DetalleProyecto = await ProjectsUpload.findOne({
        include: [
          {
            model: DocumentosSubidos,
            required: true,
          },
        ],
        where: {
          estado_aprobacion: "PENDIENTE",
          id_proyecto: req.body.id_proyecto,
          estado: "Activo",
        },
      });
      if (DetalleProyecto !== null) {
        res.status(200).json({
          historial: DetalleProyecto,
          info: InfoProject,
        });
      } else {
        res.status(404).json({
          msg: "El proyecto seleccionado no cuenta con un estado pendiente de aprobación ",
        });
      }
    } else {
      res.status(404).json({
        msg: "No fue posible encontrar la información basica del proyecto",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Buscar la información detallada del proyecto seleccionado
const EvaluarEstadoProject = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    const EstadoEvaluar = await DocumentosSubidos.findOne({
      where: { id: req.body.id_documento_entregado },
    });
    if (EstadoEvaluar !== null) {
      if (EstadoEvaluar.estado === null) {
        let estadoDocumento = "Rechazado";
        if (req.body.estado_aprobacion === "APROBADO") {
          estadoDocumento = "Aprobado";
        }
        await ProjectsUpload.update(
          {
            estado_aprobacion: req.body.estado_aprobacion,
          },
          { where: { id: req.body.id_proyecto_seleccionado } }
        )
          .then(async function (succes) {
            await DocumentosSubidos.update(
              {
                fecha_modificacion: fecha,
                user_modifica: req.usuario.id,
                estado: estadoDocumento,
                usuario_aprobador: req.usuario.id,
                // observacion_interna_evaluador: req.body.observacion_evaluador,
                // recomendaciones_futuras: req.body.recomendaciones_futuras,
              },
              {
                where: {
                  id: req.body.id_documento_entregado,
                },
              }
            )
              .then((corrrecto) => {
                res.status(200).json({
                  msg: "Información actualizada de forma ",
                });
              })
              .catch((error) => {
                res.status(404).json({
                  msg: "Ocurrió un problema inesperado al actualizar el proyecto de grado",
                });
              });
          })
          .catch((err) => {
            res.status(404).json({
              msg: "Ocurrió un problema al actualizar el estado del proyecto de grado",
            });
          });
      } else {
        res.status(404).json({
          msg: "El proyecto que desea evaluar ya cuenta con un estado de aprobación por favor valide los datos",
        });
      }
    } else {
      res.status(404).json({
        msg: "No fue posible encontrar la información del proyecto que desea actualizar.",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Listar proyectos de grado aprobados
const ListProjectArobados = async (req, res) => {
  try {
    const Lista = await Projects.findAll({
      include: [
        {
          model: ProjectsUpload,
          required: true,
          where: {
            estado_aprobacion: "APROBADO",
            estado: "Activo",
          },
          include: [
            {
              model: DocumentosSubidos,
              required: true,
              where: {
                estado: "Aprobado",
              },
            },
          ],
        },
      ],
      where: {
        estado: "Activo",
      },
    });
    res.status(200).json({
      lista: Lista,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Listar proyectos de grado aprobados
const UpdateEstadoPublico = async (req, res) => {
  try {
    let estado = null;
    if (req.body.nuevo_estado === "Publico") {
      estado = "SI";
    }
    await DocumentosSubidos.update(
      {
        publicar: estado,
        user_modifica: req.usuario.id,
      },
      {
        where: { id: req.body.id_documento_subido },
      }
    )
      .then((succes) => {
        res.status(200).json({
          msg: "Estado de proyecto actualizado de forma correcta ",
        });
      })
      .catch((err) => {
        res.status(404).json({
          msg: err.message,
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Listar proyectos de grado aprobados
const ListProjecPublicados = async (req, res) => {
  try {
    const Lista = await Projects.findAll({
      include: [
        {
          model: ProjectsUpload,
          required: true,
          where: {
            estado_aprobacion: "APROBADO",
            estado: "Activo",
          },
          include: [
            {
              model: DocumentosSubidos,
              required: true,
              where: {
                estado: "Aprobado",
                publicar: "SI",
              },
            },
          ],
        },
      ],
      where: {
        estado: "Activo",
      },
    });
    res.status(200).json({
      lista: Lista,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Detalle proyecto de grado aprobado y publicado
const DetalleProjecPublicados = async (req, res) => {
  try {
    const Lista = await Projects.findOne({
      include: [
        {
          model: ProjectsUpload,
          required: true,
          where: {
            estado_aprobacion: "APROBADO",
            estado: "Activo",
          },
          include: [
            {
              model: DocumentosSubidos,
              required: true,
              where: {
                estado: "Aprobado",
                publicar: "SI",
                id: req.params.id,
              },
              include: [
                {
                  attributes:['recomendaciones'],
                  model: NotasProyecto,
                  required: true,
                  as: "NotasProyectosFinal",
                  where:{nota: 'APROBADO'}
                },
              ],
            },
          ],
        },
      ],
      where: {
        estado: "Activo",
      },
    });
    const Estudiantes = await Projects.findOne({
      include: [
        {
          model: StudentProjects,
          required: true,
          include: [
            {
              model: Usuarios,
              required: true,
            },
          ],
        },
        {
          model: EvaluadoresProyectos,
          required: true,
          as: "EvaluadoresAnteProyecto",
          where: {tipo: 'PROYECTO FINAL' },
          include:[{
            model:Usuarios,
            required: true,
            as: 'NameEvaluadores'
          },
          {
            model:Usuarios,
            required: false,
            as: 'NameDirector'
          }
          ]
        },
      ],
      where: {
        id: Lista.id,
      },
    });
    res.status(200).json({
      lista: Lista,
      estudiante: Estudiantes,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Actualizar contraseña y enviar la nueva via correo elecronico
const UpdatePassword = async (req, res) => {
  try {
    const user = await Usuarios.findOne({
      attribute: ["nombre", "apellidos", "user", "email", "estado"],
      where: { user: req.body.user },
    });
    if (user !== null) {
      if (user.estado === "Activo") {
        let cadena = Math.random().toString(36).substring(0, 8);
        let PassworEncryptada = hashSync(cadena, genSaltSync(10));
        const mailOptions = {
          from: user.email,
          to: user.email,
          subject: "Restablecimiento de contraseña",
          text:
            "Se ha restablecido la contraseña de forma exitosa. Su contraseña temporal es:  " +
            cadena,
        };
        sendMail(mailOptions);
        await Usuarios.update(
          {
            password: PassworEncryptada,
            cambio_password: "SI",
          },
          { where: { user: req.body.user } }
        );
        res.status(200).json({
          msg:
            "La contraseña ya ha sido restablecido y enviada al correo " +
            user.email,
        });
      } else {
        res.status(404).json({
          msg: "No se pudo restablecer la contraseña ya que su usuario se encuentra inactivo",
        });
      }
    } else {
      res.status(404).json({
        msg:
          "No se encontro información relacionado al usuario " + req.body.user,
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Agregar una nueva contraseña y habilitar el usuario para el ingreso del sistema
const NewPassword = async (req, res) => {
  try {
    const user = await Usuarios.findOne({
      attribute: ["nombre", "apellidos", "user", "email", "estado"],
      where: { id: req.body.id },
    });
    if (user !== null) {
      if (user.estado === "Activo") {
        let fecha = moment().format("YYYY-MM-DD HH:mm");
        let PassworEncryptada = hashSync(req.body.password, genSaltSync(10));
        await Usuarios.update(
          {
            password: PassworEncryptada,
            cambio_password: null,
            fecha_modificacion: fecha,
          },
          { where: { id: req.body.id } }
        );
        res.status(200).json({
          msg: "La contraseña ha cambiado de forma correcta",
        });
      } else {
        res.status(404).json({
          msg: "No se pudo restablecer la contraseña ya que su usuario se encuentra inactivo",
        });
      }
    } else {
      res.status(404).json({
        msg: "No se encontro información relacionado al usuario ",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Detalle de los proyectos subidos y pendientes subidos para
const DetalleAnteProjectsPersonal = async (req, res) => {
  try {
    // Informacion del ante proyecto
    const InfoProject = await Projects.findOne({
      include: [
        {
          model: StudentProjects,
          required: true,
          where: { id_estudiante: req.usuario.id },
        },
      ],
      where: { estado: "Activo"}
    });
    if (InfoProject !== null) {
      const DetalleProyecto = await AnteProjectsUpload.findOne({
        include: [
          {
            model: DocumentosSubidos,
            required: true,
            as: "DocumentoAnteProyecto",
            where: { tipo: "ANTE PROYECTO" },
            include: [
              {
                model: NotasAnteProyecto,
                required: false,
                as: "NotasAnteProyectos",
              },
            ],
          },
        ],
        where: { estado: "Activo", id_proyecto: InfoProject.id },
      });
      res.status(200).json({
        historial: DetalleProyecto,
        info: InfoProject,
      });
    } else {
      res.status(404).json({
        msg: "El estudiante no cuenta con ante proyectos asignados para subir",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Crear el documento y subirlo a la base de datos y crear el acceso para posteriormete ser consultado
const UploadAnteProjects = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    await CreateDocumento(req, res)
      .then(async function (succes) {
        const ExisteProyecto = await AnteProjectsUpload.findOne({
          where: { id_proyecto: req.params.idproject },
        });
        if (ExisteProyecto !== null) {
          await AnteProjectsUpload.update(
            {
              fecha_modificacion: fecha,
              user_modifica: req.usuario.id,
              estado_aprobacion: "PENDIENTE",
            },
            { where: { id: ExisteProyecto.id } }
          )
            .then(async function (correcto) {
              let reqPath = path.join(__dirname, "../uploads/");
              let ruta = reqPath + succes;
              await DocumentosSubidos.create({
                fecha_creacion: fecha,
                user_entrego: req.usuario.id,
                url_documento: ruta,
                id_ante_proyecto: ExisteProyecto.id,
                descripcion: req.params.observaciones,
                tipo: "ANTE PROYECTO",
              })
                .then((exito) => {
                  res.status(200).json({
                    msg: "Documento subido de forma exitosa",
                    bandera: "BIEN",
                  });
                })
                .catch((errores) => {
                  res.status(404).json({
                    msg: " Ocurrió un problema al relacionar el proyecto",
                    bandera: "MAL",
                    info: errores,
                  });
                });
            })
            .catch((err) => {
              console.log(err);
              res.status(404).json({
                msg: " Ocurrió un problema al editar el registro del proyecto",
                bandera: "MAL",
                info: err,
              });
            });
        } else {
          await AnteProjectsUpload.create({
            fecha_creacion: fecha,
            user_crea: req.usuario.id,
            id_proyecto: req.params.idproject,
            estado: "Activo",
            estado_aprobacion: "PENDIENTE",
          })
            .then(async function (datos) {
              const idProject = datos.id;
              let reqPath = path.join(__dirname, "../uploads/");
              let ruta = reqPath + succes;
              await DocumentosSubidos.create({
                fecha_creacion: fecha,
                user_entrego: req.usuario.id,
                url_documento: ruta,
                id_ante_proyecto: idProject,
                descripcion: req.params.observaciones,
                tipo: "ANTE PROYECTO",
              })
                .then((exito) => {
                  res.status(200).json({
                    msg: "Documento subido de forma exitosa",
                    bandera: "BIEN",
                  });
                })
                .catch((errores) => {
                  console.log(errores);
                  res.status(404).json({
                    msg: " Ocurrió un problema al relacionar el proyecto",
                    bandera: "MAL",
                    info: errores,
                  });
                });
            })
            .catch((error) => {
              console.log(error);
              res.status(404).json({
                msg: " Ocurrió un problema al crear el registro del proyecto",
                bandera: "MAL",
                info: error,
              });
            });
        }
      })
      .catch((error) => {
        res.status(404).json({
          msg: " Ocurrió un problema inesperado",
          bandera: "MAL",
          info: error,
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Listar ante proyectos pendientes por evaluar
const ListAnteProjectEvaluar = async (req, res) => {
  try {
    const Lista = await Projects.findAll({
      include: [
        {
          model: StudentProjects,
          required: true,
        },
        {
          model: AnteProjectsUpload,
          required: true,
          as: "AnteProyectosSubidos",
          where: { estado_aprobacion: "PENDIENTE" },
        },
        {
          model: EvaluadoresProyectos,
          required: true,
          as: "EvaluadoresAnteProyecto",
          where: { id_evaluador: req.usuario.id,tipo: 'ANTE PROYECTO' },
        },
      ],
    });
    res.status(200).json({
      lista: Lista,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Buscar la información detallada del proyecto seleccionado
const DetalleAnteProjectEvaluar = async (req, res) => {
  try {
    const DetalleAnteProyecto = await AnteProjectsUpload.findOne({
      include: [
        {
          model: DocumentosSubidos,
          required: true,
          as: "DocumentoAnteProyecto",
          where: { tipo: "ANTE PROYECTO" },
          include: [
            {
              model: NotasAnteProyecto,
              required: false,
              as: "NotasAnteProyectos",
            },
          ],
        },
      ],
      where: {
        estado: "Activo",
        id: req.body.id_proyecto,
        estado_aprobacion: "PENDIENTE",
      },
    });
    if (DetalleAnteProyecto !== null) {
      const ProjectEvaluadores = await Projects.findOne({
        include: [
          {
            model: AnteProjectsUpload,
            required: true,
            as: "AnteProyectosSubidos",
            where: { estado_aprobacion: "PENDIENTE" },
          },
          {
            model: EvaluadoresProyectos,
            required: true,
            as: "EvaluadoresAnteProyecto",
          },
        ],
        where: { id: DetalleAnteProyecto.id_proyecto },
      });
      res.status(200).json({
        DetalleAnteProyecto: DetalleAnteProyecto,
        ProjectEvaluadores: ProjectEvaluadores,
      });
    } else {
      res.status(404).json({
        msg: "El ante proyecto no cuenta con un estado pendiente por evaluar",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Evaluar el ante proyecto
const EvaluarAntePoyecto = async (req, res) => {
  try {
    // fecha actal
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    // Numero total de evaluadores
    const TotalEvaluadores = await EvaluadoresProyectos.count({
      where: {
        tipo: "ANTE PROYECTO",
        id_proyecto: req.body.id_proyecto_total,
      },
    });
    // Total de notas ya ingresadas
    const TotalNotas = await NotasAnteProyecto.findAll({
      where: {
        id_ante_proyecto: req.body.id_project,
        id_documento: req.body.id_documento_entregado,
      },
    });
    if (TotalEvaluadores > 0) {
      // Determina si se va a ingresar estado final
      let InterpretacionNota = null;
      let EstadoDocumento = null;
      if (TotalEvaluadores === 1) {
        if (TotalNotas.length === 0) {
          switch (req.body.nota_evaluador) {
            case "APROBADO CON CORRECCIONES":
              InterpretacionNota = "CORREGIR";
              EstadoDocumento = "Rechazado";
              break;
            case "PENDIENTE DE APROBACION":
              InterpretacionNota = "CORREGIR";
              EstadoDocumento = "Rechazado";
              break;
            case "APROBADO":
              InterpretacionNota = "APROBADO";
              EstadoDocumento = "Aprobado";
              break;
            case "NO APROBADO":
              InterpretacionNota = "RECHAZADO";
              EstadoDocumento = "Rechazado";
              break;
            case "ANULADO":
              InterpretacionNota = "RECHAZADO";
              EstadoDocumento = "Rechazado";
              break;
            default:
              break;
          }
          await NotasAnteProyecto.create({
            fecha_creacion: fecha,
            usuario_crea: req.usuario.id,
            id_ante_proyecto: req.body.id_project,
            nota: req.body.nota_evaluador,
            observacion_interna_evaluador: req.body.observacion_evaluador,
            id_documento: req.body.id_documento_entregado,
            recomendaciones: req.body.recomendaciones_futuras,
          });
          await DocumentosSubidos.update(
            {
              fecha_modificacion: fecha,
              user_modifica: req.usuario.id,
              estado: EstadoDocumento,
            },
            { where: { id: req.body.id_documento_entregado } }
          );
          await AnteProjectsUpload.update(
            {
              estado_aprobacion: InterpretacionNota,
            },
            { where: { id: req.body.id_project } }
          );
          if (InterpretacionNota === "APROBADO") {
            await Projects.update(
              {
                etapa: "PROYECTO FINAL",
              },
              { where: { id: req.body.id_proyecto_total } }
            );
          }
          res.status(200).json({
            msg: "Nota de ante proyecto almacenada de forma correcta",
          });
        } else {
          res.status(404).json({
            msg: "Este proyecto ya cuenta con una calificación ",
          });
        }
      } else {
        if (TotalNotas.length > 0) {
          // Valida si ya existe dos notas entonces el tercer evaluador determinara la nota final
          if (TotalNotas.length === 2) {
            switch (req.body.nota_evaluador) {
              case "APROBADO CON CORRECCIONES":
                InterpretacionNota = "CORREGIR";
                EstadoDocumento = "Rechazado";
                break;
              case "APROBADO":
                InterpretacionNota = "APROBADO";
                EstadoDocumento = "Aprobado";
                break;
              case "PENDIENTE DE APROBACION":
                InterpretacionNota = "CORREGIR";
                EstadoDocumento = "Rechazado";
                break;
              case "NO APROBADO":
                InterpretacionNota = "RECHAZADO";
                EstadoDocumento = "Rechazado";
                break;
              case "ANULADO":
                InterpretacionNota = "RECHAZADO";
                EstadoDocumento = "Rechazado";
                break;
              default:
                break;
            }
            await NotasAnteProyecto.create({
              fecha_creacion: fecha,
              usuario_crea: req.usuario.id,
              id_ante_proyecto: req.body.id_project,
              nota: req.body.nota_evaluador,
              observacion_interna_evaluador: req.body.observacion_evaluador,
              id_documento: req.body.id_documento_entregado,
              recomendaciones: req.body.recomendaciones_futuras,
            });
            await DocumentosSubidos.update(
              {
                fecha_modificacion: fecha,
                user_modifica: req.usuario.id,
                estado: EstadoDocumento,
              },
              { where: { id: req.body.id_documento_entregado } }
            );
            await AnteProjectsUpload.update(
              {
                estado_aprobacion: InterpretacionNota,
              },
              { where: { id: req.body.id_project } }
            );
            if (InterpretacionNota === "APROBADO") {
              await Projects.update(
                {
                  etapa: "PROYECTO FINAL",
                },
                { where: { id: req.body.id_proyecto_total } }
              );
            }
            res.status(200).json({
              msg: "Nota de ante proyecto almacenada de forma correcta",
            });
          } else {
            if (TotalNotas[0].nota === req.body.nota_evaluador) {
              switch (req.body.nota_evaluador) {
                case "APROBADO CON CORRECCIONES":
                  InterpretacionNota = "CORREGIR";
                  EstadoDocumento = "Rechazado";
                  break;
                case "PENDIENTE DE APROBACION":
                  InterpretacionNota = "CORREGIR";
                  EstadoDocumento = "Rechazado";
                  break;
                case "APROBADO":
                  InterpretacionNota = "APROBADO";
                  EstadoDocumento = "Aprobado";
                  break;
                case "NO APROBADO":
                  InterpretacionNota = "RECHAZADO";
                  EstadoDocumento = "Rechazado";
                  break;
                case "ANULADO":
                  InterpretacionNota = "RECHAZADO";
                  EstadoDocumento = "Rechazado";
                  break;
                default:
                  break;
              }
              await NotasAnteProyecto.create({
                fecha_creacion: fecha,
                usuario_crea: req.usuario.id,
                id_ante_proyecto: req.body.id_project,
                nota: req.body.nota_evaluador,
                observacion_interna_evaluador: req.body.observacion_evaluador,
                id_documento: req.body.id_documento_entregado,
                recomendaciones: req.body.recomendaciones_futuras,
              });
              await DocumentosSubidos.update(
                {
                  fecha_modificacion: fecha,
                  user_modifica: req.usuario.id,
                  estado: EstadoDocumento,
                },
                { where: { id: req.body.id_documento_entregado } }
              );
              await AnteProjectsUpload.update(
                {
                  estado_aprobacion: InterpretacionNota,
                },
                { where: { id: req.body.id_project } }
              );
              if (InterpretacionNota === "APROBADO") {
                await Projects.update(
                  {
                    etapa: "PROYECTO FINAL",
                  },
                  { where: { id: req.body.id_proyecto_total } }
                );
              }
              res.status(200).json({
                msg: "Nota de ante proyecto almacenada de forma correcta",
              });
            } else {
              await NotasAnteProyecto.create({
                fecha_creacion: fecha,
                usuario_crea: req.usuario.id,
                id_ante_proyecto: req.body.id_project,
                nota: req.body.nota_evaluador,
                observacion_interna_evaluador: req.body.observacion_evaluador,
                id_documento: req.body.id_documento_entregado,
                recomendaciones: req.body.recomendaciones_futuras,
              });
              res.status(200).json({
                msg: "Nota de ante proyecto almacenada de forma correcta",
              });
            }
          }
        } else {
          await NotasAnteProyecto.create({
            fecha_creacion: fecha,
            usuario_crea: req.usuario.id,
            id_ante_proyecto: req.body.id_project,
            nota: req.body.nota_evaluador,
            observacion_interna_evaluador: req.body.observacion_evaluador,
            id_documento: req.body.id_documento_entregado,
            recomendaciones: req.body.recomendaciones_futuras,
          });
          res.status(200).json({
            msg: "Nota de ante proyecto almacenada de forma correcta",
          });
        }
      }
    } else {
      res.status(404).json({
        msg: "Este proyecto no cuenta con evaluadores asignados",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Detalle de los proyectos subidos y pendientes subidos para
const DetalleProjectsFinalPersonal = async (req, res) => {
  try {
    // Informacion del ante proyecto
    const InfoProject = await Projects.findOne({
      include: [
        {
          model: StudentProjects,
          required: true,
          where: { id_estudiante: req.usuario.id },
        },
      ],
      where:{etapa: ['PROYECTO FINAL','PROYECTO APROBADO'], estado: 'Activo'}
    });
    if (InfoProject !== null) {
      const DetalleProyecto = await ProjectsUpload.findOne({
        include: [
          {
            model: DocumentosSubidos,
            required: true,
            as: "DocumentoProyectoProyectoFinal",
            where: { tipo: "PROYECTO FINAL" },
            include: [
              {
                model: NotasProyecto,
                required: false,
                as: "NotasProyectosFinal",
              }, 
            ],
          },
        ],
        where: { estado: "Activo", id_proyecto: InfoProject.id },
      });
      const DetalleProyectoUpload = await ProjectsUpload.findOne({
        where: { estado: "Activo", id_proyecto: InfoProject.id },
      });
      if(DetalleProyectoUpload !== null){
        if(DetalleProyectoUpload.id_ante_proyecto_aprobado === null){
          const AntePproyectoAprobado = await AnteProjectsUpload.findOne({
            where: {
              estado_aprobacion: "APROBADO",
              id_proyecto: InfoProject.id,
            },
          });
          await ProjectsUpload.update(
            {
              id_ante_proyecto_aprobado: AntePproyectoAprobado.id,
            },
            { where: { id: DetalleProyectoUpload.id } }
          );
        }
      }
      const RutaAnteProyectoAprobado = await AnteProjectsUpload.findOne({
        include: [
          {
            model: DocumentosSubidos,
            required: true,
            as: "DocumentoAnteProyecto",
            where: { tipo: "ANTE PROYECTO",estado:'Aprobado' },
          }
        ],
        where: {
          estado_aprobacion: "APROBADO",
          id_proyecto: InfoProject.id,
        },
      });
        res.status(200).json({
          historial: DetalleProyecto,
          info: InfoProject,
          urlAnteProyecto: RutaAnteProyectoAprobado,
        });

    } else {
      res.status(404).json({
        msg: "El estudiante no cuenta con proyectos asignados para subir",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Crear el documento y subirlo a la base de datos y crear el acceso para posteriormete ser consultado
const UploadProjectsFinal = async (req, res) => {
  try {
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    await CreateDocumento(req, res)
      .then(async function (succes) {
        const ExisteProyecto = await ProjectsUpload.findOne({
          where: { id_proyecto: req.params.idproject },
        });
        if (ExisteProyecto !== null) {
          await ProjectsUpload.update(
            {
              fecha_modificacion: fecha,
              user_modifica: req.usuario.id,
              estado_aprobacion: "PENDIENTE",
            },
            { where: { id: ExisteProyecto.id } }
          )
            .then(async function (correcto) {
              let reqPath = path.join(__dirname, "../uploads/");
              let ruta = reqPath + succes;
              await DocumentosSubidos.create({
                fecha_creacion: fecha,
                user_entrego: req.usuario.id,
                url_documento: ruta,
                id_proyecto_subido: ExisteProyecto.id,
                descripcion: req.params.observaciones,
                tipo: "PROYECTO FINAL",
              })
                .then(async (exito) => {
                  const DetalleProyectoUpload = await ProjectsUpload.findOne({
                    where: { estado: "Activo", id_proyecto: req.params.idproject },
                  });
                  if(DetalleProyectoUpload !== null){
                    if(DetalleProyectoUpload.id_ante_proyecto_aprobado === null){
                      const AntePproyectoAprobado = await AnteProjectsUpload.findOne({
                        where: {
                          estado_aprobacion: "APROBADO",
                          id_proyecto: req.params.idproject,
                        },
                      });
                      await ProjectsUpload.update(
                        {
                          id_ante_proyecto_aprobado: AntePproyectoAprobado.id,
                        },
                        { where: { id: DetalleProyectoUpload.id } }
                      );
                    }
                  }
                  res.status(200).json({
                    msg: "Documento subido de forma exitosa",
                    bandera: "BIEN",
                  });
                })
                .catch((errores) => {
                  res.status(404).json({
                    msg: " Ocurrió un problema al relacionar el proyecto",
                    bandera: "MAL",
                    info: errores,
                  });
                });
            })
            .catch((err) => {
              console.log(err);
              res.status(404).json({
                msg: " Ocurrió un problema al editar el registro del proyecto",
                bandera: "MAL",
                info: err,
              });
            });
        } else {
          await ProjectsUpload.create({
            fecha_creacion: fecha,
            user_crea: req.usuario.id,
            id_proyecto: req.params.idproject,
            estado: "Activo",
            estado_aprobacion: "PENDIENTE",
          })
            .then(async function (datos) {
              const idProject = datos.id;
              let reqPath = path.join(__dirname, "../uploads/");
              let ruta = reqPath + succes;
              await DocumentosSubidos.create({
                fecha_creacion: fecha,
                user_entrego: req.usuario.id,
                url_documento: ruta,
                id_proyecto_subido: idProject,
                descripcion: req.params.observaciones,
                tipo: "PROYECTO FINAL",
              })
                .then((exito) => {
                  res.status(200).json({
                    msg: "Documento subido de forma exitosa",
                    bandera: "BIEN",
                  });
                })
                .catch((errores) => {
                  console.log(errores);
                  res.status(404).json({
                    msg: " Ocurrió un problema al relacionar el proyecto",
                    bandera: "MAL",
                    info: errores,
                  });
                });
            })
            .catch((error) => {
              console.log(error);
              res.status(404).json({
                msg: " Ocurrió un problema al crear el registro del proyecto",
                bandera: "MAL",
                info: error,
              });
            });
        }
      })
      .catch((error) => {
        res.status(404).json({
          msg: " Ocurrió un problema inesperado",
          bandera: "MAL",
          info: error,
        });
      });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Listar ante proyectos pendientes por evaluar
const ListProjectFinalEvaluar = async (req, res) => {
  try {
    const Lista = await Projects.findAll({
      include: [
        {
          model: StudentProjects,
          required: true,
        },
        {
          model: ProjectsUpload,
          required: true,
          as: "ProyectosFinalesSubidos",
          where: { estado_aprobacion: "PENDIENTE" },
        },
        {
          model: EvaluadoresProyectos,
          required: true,
          as: "EvaluadoresAnteProyecto",
          where: { id_evaluador: req.usuario.id,tipo:'PROYECTO FINAL' },
        },
      ],
    });
    res.status(200).json({
      lista: Lista,
    });
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Buscar la información detallada del proyecto seleccionado
const DetalleProjectFinalEvaluar = async (req, res) => {
  try {
    const DetalleAnteProyecto = await ProjectsUpload.findOne({
      include: [
        {
          model: DocumentosSubidos,
          required: true,
          as: "DocumentoProyectoFinal",
          //  DocumentoAnteProyecto
          where: { tipo: "PROYECTO FINAL" },
          include: [
            {
              model: NotasProyecto,
              required: false,
              as: "NotasProyectosFinal",
              // NotasAnteProyectos   
            },
          ],
        },
      ],
      where: {
        estado: "Activo",
        id: req.body.id_proyecto,
        estado_aprobacion: "PENDIENTE",
      },
    });
    if (DetalleAnteProyecto !== null) {
      const ProjectEvaluadores = await Projects.findOne({
        include: [
          {
            model: ProjectsUpload,
            required: true,
            as: "ProyectosFinalesSubidos",
            // AnteProyectosSubidos 
            where: { estado_aprobacion: "PENDIENTE" },
          },
          {
            model: EvaluadoresProyectos,
            required: true,
            as: "EvaluadoresAnteProyecto",
          },
        ],
        where: { id: DetalleAnteProyecto.id_proyecto },
      });
      const RutaAnteProyectoAprobado = await AnteProjectsUpload.findOne({
        include: [
          {
            model: DocumentosSubidos,
            required: true,
            as: "DocumentoAnteProyecto",
            where: { tipo: "ANTE PROYECTO",estado:'Aprobado' }, 
          }
        ],
        where: { 
          estado_aprobacion: "APROBADO",
          id: DetalleAnteProyecto.id_ante_proyecto_aprobado 
        },
      });
      res.status(200).json({  
        DetalleAnteProyecto: DetalleAnteProyecto,
        ProjectEvaluadores: ProjectEvaluadores,
        UrlAnteProyecto: RutaAnteProyectoAprobado.DocumentoAnteProyecto[0].url_documento
      });
    } else {
      res.status(404).json({
        msg: "El ante proyecto no cuenta con un estado pendiente por evaluar",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
// Evaluar el proyecto final
const EvaluarPoyectoFinal = async (req, res) => {
  try {
    // fecha actal
    let fecha = moment().format("YYYY-MM-DD HH:mm");
    // Numero total de evaluadores
    const TotalEvaluadores = await EvaluadoresProyectos.count({
      where: {
        tipo: "PROYECTO FINAL",
        id_proyecto: req.body.id_proyecto_total,
      },
    });
    // Total de notas ya ingresadas
    const TotalNotas = await NotasProyecto.findAll({
      where: {
        id_proyecto: req.body.id_project,
        id_documento: req.body.id_documento_entregado,
      },
    });
    if (TotalEvaluadores > 0) {
      // Determina si se va a ingresar estado final
      let InterpretacionNota = null;
      let EstadoDocumento = null;
      if (TotalEvaluadores === 1) {
        if (TotalNotas.length === 0) {
          switch (req.body.nota_evaluador) {
            case "APROBADO CON CORRECCIONES":
              InterpretacionNota = "CORREGIR";
              EstadoDocumento = "Rechazado";
              break;
            case "PENDIENTE DE APROBACION":
              InterpretacionNota = "CORREGIR";
              EstadoDocumento = "Rechazado";
              break;
            case "APROBADO":
              InterpretacionNota = "APROBADO";
              EstadoDocumento = "Aprobado";
              break;
            case "NO APROBADO":
              InterpretacionNota = "RECHAZADO";
              EstadoDocumento = "Rechazado";
              break;
            case "ANULADO":
              InterpretacionNota = "RECHAZADO";
              EstadoDocumento = "Rechazado";
              break;
            default:
              break;
          }
          await NotasProyecto.create({
            fecha_creacion: fecha,
            usuario_crea: req.usuario.id,
            id_proyecto: req.body.id_project,
            nota: req.body.nota_evaluador,
            observacion_interna_evaluador: req.body.observacion_evaluador,
            id_documento: req.body.id_documento_entregado,
            recomendaciones: req.body.recomendaciones_futuras,
          });
          await DocumentosSubidos.update(
            {
              fecha_modificacion: fecha,
              user_modifica: req.usuario.id,
              estado: EstadoDocumento,
            },
            { where: { id: req.body.id_documento_entregado } }
          );
          await ProjectsUpload.update(
            {
              estado_aprobacion: InterpretacionNota,
            },
            { where: { id: req.body.id_project } }
          );
          if (InterpretacionNota === "APROBADO") {
            await Projects.update(
              {
                etapa: "PROYECTO APROBADO",
              },
              { where: { id: req.body.id_proyecto_total } }
            );
          }
          res.status(200).json({
            msg: "Nota de ante proyecto almacenada de forma correcta",
          });
        } else {
          res.status(404).json({
            msg: "Este proyecto ya cuenta con una calificación ",
          });
        }
      } else {
        if (TotalNotas.length > 0) {
          // Valida si ya existe dos notas entonces el tercer evaluador determinara la nota final
          if (TotalNotas.length === 2) {
            switch (req.body.nota_evaluador) {
              case "APROBADO CON CORRECCIONES":
                InterpretacionNota = "CORREGIR";
                EstadoDocumento = "Rechazado";
                break;
              case "APROBADO":
                InterpretacionNota = "APROBADO";
                EstadoDocumento = "Aprobado";
                break;
              case "PENDIENTE DE APROBACION":
                InterpretacionNota = "CORREGIR";
                EstadoDocumento = "Rechazado";
                break;
              case "NO APROBADO":
                InterpretacionNota = "RECHAZADO";
                EstadoDocumento = "Rechazado";
                break;
              case "ANULADO":
                InterpretacionNota = "RECHAZADO";
                EstadoDocumento = "Rechazado";
                break;
              default:
                break;
            }
            await NotasProyecto.create({
              fecha_creacion: fecha,
              usuario_crea: req.usuario.id,
              id_proyecto: req.body.id_project,
              nota: req.body.nota_evaluador,
              observacion_interna_evaluador: req.body.observacion_evaluador,
              id_documento: req.body.id_documento_entregado,
              recomendaciones: req.body.recomendaciones_futuras,
            });
            await DocumentosSubidos.update(
              {
                fecha_modificacion: fecha,
                user_modifica: req.usuario.id,
                estado: EstadoDocumento,
              },
              { where: { id: req.body.id_documento_entregado } }
            );
            await ProjectsUpload.update(
              {
                estado_aprobacion: InterpretacionNota,
              },
              { where: { id: req.body.id_project } }
            );
            if (InterpretacionNota === "APROBADO") {
              await Projects.update(
                {
                  etapa: "PROYECTO APROBADO",
                },
                { where: { id: req.body.id_proyecto_total } }
              );
            }
            res.status(200).json({
              msg: "Nota de ante proyecto almacenada de forma correcta",
            });
          } else {
            if (TotalNotas[0].nota === req.body.nota_evaluador) {
              switch (req.body.nota_evaluador) {
                case "APROBADO CON CORRECCIONES":
                  InterpretacionNota = "CORREGIR";
                  EstadoDocumento = "Rechazado";
                  break;
                case "PENDIENTE DE APROBACION":
                  InterpretacionNota = "CORREGIR";
                  EstadoDocumento = "Rechazado";
                  break;
                case "APROBADO":
                  InterpretacionNota = "APROBADO";
                  EstadoDocumento = "Aprobado";
                  break;
                case "NO APROBADO":
                  InterpretacionNota = "RECHAZADO";
                  EstadoDocumento = "Rechazado";
                  break;
                case "ANULADO":
                  InterpretacionNota = "RECHAZADO";
                  EstadoDocumento = "Rechazado";
                  break;
                default:
                  break;
              }
              await NotasProyecto.create({
                fecha_creacion: fecha,
                usuario_crea: req.usuario.id,
                id_proyecto: req.body.id_project,
                nota: req.body.nota_evaluador,
                observacion_interna_evaluador: req.body.observacion_evaluador,
                id_documento: req.body.id_documento_entregado,
                recomendaciones: req.body.recomendaciones_futuras,
              });
              await DocumentosSubidos.update(
                {
                  fecha_modificacion: fecha,
                  user_modifica: req.usuario.id,
                  estado: EstadoDocumento,
                },
                { where: { id: req.body.id_documento_entregado } }
              );
              await ProjectsUpload.update(
                {
                  estado_aprobacion: InterpretacionNota,
                },
                { where: { id: req.body.id_project } }
              );
              if (InterpretacionNota === "APROBADO") {
                await Projects.update(
                  {
                    etapa: "PROYECTO APROBADO",
                  },
                  { where: { id: req.body.id_proyecto_total } }
                );
              }
              res.status(200).json({
                msg: "Nota de ante proyecto almacenada de forma correcta",
              });
            } else {
              await NotasProyecto.create({
                fecha_creacion: fecha,
                usuario_crea: req.usuario.id,
                id_proyecto: req.body.id_project,
                nota: req.body.nota_evaluador,
                observacion_interna_evaluador: req.body.observacion_evaluador,
                id_documento: req.body.id_documento_entregado,
                recomendaciones: req.body.recomendaciones_futuras,
              });
              res.status(200).json({
                msg: "Nota de ante proyecto almacenada de forma correcta",
              });
            }
          }
        } else {
          await NotasProyecto.create({
            fecha_creacion: fecha,
            usuario_crea: req.usuario.id,
            id_proyecto : req.body.id_project,
            nota: req.body.nota_evaluador,
            observacion_interna_evaluador: req.body.observacion_evaluador,
            id_documento: req.body.id_documento_entregado,
            recomendaciones: req.body.recomendaciones_futuras,
          });
          res.status(200).json({
            msg: "Nota de ante proyecto almacenada de forma correcta",
          });
        }
      }
    } else {
      res.status(404).json({
        msg: "Este proyecto no cuenta con evaluadores asignados",
      });
    }
  } catch (error) {
    res.status(404).json({
      msg: error.message,
    });
  }
};
module.exports = {
  IniciarSesion,
  ListUserRoles,
  SaveUsuer,
  UpdateUsuer,
  ListStudentProjects,
  SaveProjects,
  UpdateProjects,
  DeleteProjects,
  DetalleProjectsPersonal,
  UploadProjects,
  ListProjectEvaluar,
  DetalleProjectEvaluar,
  EvaluarEstadoProject,
  ListProjectArobados,
  UpdateEstadoPublico,
  ListProjecPublicados,
  DetalleProjecPublicados,
  UpdatePassword,
  NewPassword,
  DetalleAnteProjectsPersonal,
  UploadAnteProjects,
  ListAnteProjectEvaluar,
  DetalleAnteProjectEvaluar,
  EvaluarAntePoyecto,
  DetalleProjectsFinalPersonal,
  UploadProjectsFinal,
  ListProjectFinalEvaluar,
  DetalleProjectFinalEvaluar,
  EvaluarPoyectoFinal,
};
